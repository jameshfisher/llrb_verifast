#ifndef SPEC_INSERT_FIXPOINT_H
#define SPEC_INSERT_FIXPOINT_H

#include "../index.h"

/*@
// <index_insert>
fixpoint list<Pair<int,V> > index_insert<V>(int k, V v, list<Pair<int,V> > I0)
{
  switch (I0) {
    case nil: return cons(Pair(k,v), nil);
    case cons(e0, I1): return
      switch (e0) {
        case Pair(k0, v0): return
          ( k < k0 ? cons(Pair(k,v), I0)             // Insert new pair at start
          : k > k0 ? cons(e0, index_insert(k, v, I1))  // Insert into rest
          :          cons(Pair(k, v), I1)            // Replace old value with new
          );
      };
  }
}
// </index_insert>

// <index_insert_2>
fixpoint list<Pair<int,V> > index_insert_2<V>(int k, V v, list<Pair<int,V> > I0)
{
  return cons(Pair(k,v), I0);
}
// </>
@*/

#endif
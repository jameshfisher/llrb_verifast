#ifndef SEQ_INSERT_TEST_H
#define SEQ_INSERT_TEST_H

#include "./fixpoint.h"

/*@
lemma void index_insert_test()
requires emp; ensures emp;
{
  // Insert new element into nil is single-element
  assert
    index_insert(8, 5, nil)
    == cons(Pair(8,5), nil);
  
  // Insert new element into middle
  assert
    index_insert(6, 9, cons(Pair(5,0),                 cons(Pair(8,50), nil)))
    ==               cons(Pair(5,0), cons(Pair(6,9), cons(Pair(8,50), nil)));
  
  // Insert new element at end
  assert
    index_insert(10, 2, cons(Pair(5,0), cons(Pair(8,50), nil)))
    ==                cons(Pair(5,0), cons(Pair(8,50), cons(Pair(10,2), nil)));
  
  // Insert new element at start
  assert
    index_insert(1, 1,   cons(Pair(5,0), cons(Pair(8,50), nil)))
    == cons(Pair(1,1), cons(Pair(5,0), cons(Pair(8,50), nil)));
  
  // Insert existing element
  assert
    index_insert(5, 10, cons(Pair(3,5), cons(Pair(5, 2), cons(Pair(8,90), nil))))
    ==                cons(Pair(3,5), cons(Pair(5,10), cons(Pair(8,90), nil)));
}
@*/

#endif
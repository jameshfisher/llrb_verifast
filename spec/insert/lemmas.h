#ifndef LIST_INSERT_LEMMAS_H
#define LIST_INSERT_LEMMAS_H

#include "./fixpoint.h"
#include "../index.h"
#include "../lemmas.h"

/*@
// <index_insert_left>
lemma void index_insert_left<V>(int nk, V nv, list<Pair<int,V> > L0, int k, V v, list<Pair<int,V> > R0)
  requires  nk < k  &&  sorted(append(L0, cons(Pair(k,v), R0)));
  ensures   index_insert(nk, nv, append(L0, cons(Pair(k,v), R0))) == append(index_insert(nk, nv, L0), cons(Pair(k,v), R0));
{
  switch (L0) {
    case nil:
    case cons(l0, L1):
      switch (l0) {
        case Pair(k0, v0):
          if (k0 < nk) index_insert_left(nk, nv, L1, k, v, R0);
      }
  }
}
// </index_insert_left>

// <index_insert_right>
lemma void index_insert_right<V>(int nk, V nv, list<Pair<int,V> > L0, int k, V v, list<Pair<int,V> > R0)
  requires  k < nk  &&  sorted(append(L0, cons(Pair(k,v), R0)));
  ensures   index_insert(nk, nv, append(L0, cons(Pair(k,v), R0))) == append(L0, cons(Pair(k,v), index_insert(nk, nv, R0)));
{
  switch (L0) {
    case nil:
    case cons(l0, L1):
      switch (l0) {
        case Pair(k0, v0):
          left_side_of_sorted_is_bound(L0, k, v, R0);
          if (k0 < nk) index_insert_right(nk, nv, L1, k, v, R0);
      }
  }
}
// </index_insert_right>

// <index_insert_here>
lemma void index_insert_here<V>(int nk, V nv, list<Pair<int,V> > L0, int k, V v, list<Pair<int,V> > R0)
  requires  nk == k  &&  sorted(append(L0, cons(Pair(k,v), R0)));
  ensures   index_insert(nk, nv, append(L0, cons(Pair(k,v), R0))) == append(L0, cons(Pair(k,nv), R0));
{
  switch (L0) {
    case nil:
    case cons(l0, L1):
      switch(l0) {
        case Pair(k0, v0):
          left_side_of_sorted_is_bound(L0, k, v, R0);
          index_insert_here(nk, nv, L1, k, v, R0);        
      }
  }
}
// </index_insert_here>

@*/

#endif

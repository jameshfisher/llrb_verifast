#ifndef VERIFY_LIST_SEARCH_H
#define VERIFY_LIST_SEARCH_H

#include "../fixpoints.h"
#include "../lemmas.h"
#include "../search/fixpoint.h"
#include "./fixpoint.h"

/*@

// <index_insert_preserves_lowerBound>
lemma void index_insert_preserves_lowerBound<V>(int k, V v, int k0, V v0, list<Pair<int,V> > I1)
  requires sorted(cons(Pair(k0, v0), I1)) == true  &&  k0 < k;
  ensures  lowerBound(k0, index_insert(k, v, I1)) == true;
{
  switch (I1) {
    case nil:
    case cons(e1, I2):
      switch (e1) {
        case Pair(k1, v1):
          if (k1 < k) {
            index_insert_preserves_lowerBound(k, v, k1, v1, I2);
            loosen_lowerBound(k0, k1, index_insert(k, v, I2));
          }
      }
  }
}
// </index_insert_preserves_lowerBound>

// <index_insert_preserves_sorted>
lemma void index_insert_preserves_sorted<V>(int k, V v, list<Pair<int,V> > I0)
  requires sorted(I0) == true;
  ensures sorted(index_insert(k, v, I0)) == true;
{
  switch (I0) {
    case nil:
    case cons(e0, I1):
      switch (e0) {
        case Pair(k0, v0):
          if (k < k0) {
            loosen_lowerBound(k, k0, I1);
          }
          else if (k > k0) {
            index_insert_preserves_sorted(k, v, I1);
            index_insert_preserves_lowerBound(k, v, k0, v0, I1);
          }
      }
  }
}
// </index_insert_preserves_sorted>

// <search_k_in_insert_k_v_is_v>
lemma void search_k_in_insert_k_v_is_v<V>(int k, V v, list<Pair<int,V> > I0)
  requires sorted(I0) == true;
  ensures index_search(k, index_insert(k, v, I0)) == Just(v);
{
  switch (I0) {
    case nil:
    case cons(kv0, I1):
      switch (kv0) {
        case Pair(k0, v0): search_k_in_insert_k_v_is_v(k, v, I1);
      }
  }
}
// </search_k_in_insert_k_v_is_v>

// <search_non_k_in_insert_k_v_unaffected>
lemma void search_non_k_in_insert_k_v_unaffected<V>(int k, V v, list<Pair<int,V> > I0, int kn)
  requires sorted(I0) == true && kn != k;
  ensures index_search(kn, index_insert(k, v, I0)) == index_search(kn, I0);
{
  switch (I0) {
    case nil:
    case cons(kv0, I1):
      switch (kv0) {
        case Pair(k0, v0): search_non_k_in_insert_k_v_unaffected(k, v, I1, kn);
      }
  }
}
// </search_non_k_in_insert_k_v_unaffected>

// <index_insert_proof_declaration>
lemma void index_insert_proof<V>(int k, V v, list<Pair<int,V> > I0, int kn);
  requires sorted(I0) == true;
  ensures index_search(kn, index_insert(k, v, I0)) == (kn == k ? Just(v) : index_search(kn, I0));
// </index_insert_proof_declaration>

// <index_insert_proof>
lemma void index_insert_proof<V>(int k, V v, list<Pair<int,V> > I0, int kn)
  requires sorted(I0) == true;
  ensures index_search(kn, index_insert(k, v, I0)) == (kn == k ? Just(v) : index_search(kn, I0));
{
  if (kn == k)
    search_k_in_insert_k_v_is_v(k, v, I0);
  else
    search_non_k_in_insert_k_v_unaffected(k, v, I0, kn);
}
// </index_insert_proof>

// <index_insert_2_proof>
lemma void index_insert_2_proof<V>(int k, V v, list<Pair<int,V> > I0, int kn)
  requires true;
  ensures index_search(kn, index_insert_2(k, v, I0)) == (kn == k ? Just(v) : index_search(kn, I0));
{
}
// </>
@*/

#endif

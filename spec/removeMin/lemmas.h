#ifndef REMOVEMIN_LEMMAS_H
#define REMOVEMIN_LEMMAS_H

#include "../index.h"

/*@
// <leftmost_of_left_is_leftmost_of_whole>
lemma void leftmost_of_left_is_leftmost_of_whole<V>(list<Pair<int,V> > I0, list<Pair<int,V> > L0, int k, V v, list<Pair<int,V> > R0)
  requires I0 == append(L0, cons(Pair(k,v), R0))  &*&  Cons(I0, ?e0, ?I1)  &*&  Cons(L0, ?l0, ?L1);
  ensures  l0 == e0                               &*&  Cons(I0,  e0,  I1)  &*&  Cons(L0,  l0,  L1);
{
  open Cons(I0, e0, I1);
  open Cons(L0, l0, L1);
  close Cons(I0, e0, I1);
  close Cons(L0, l0, L1);
}
// </leftmost_of_left_is_leftmost_of_whole>

// <removeMin_left_is_removeMin_root>
lemma void removeMin_left_is_removeMin_root<V>(list<Pair<int,V> > I0, list<Pair<int,V> > L0, int k, V v, list<Pair<int,V> > R0)
  requires Cons(I0, ?e0, ?I1)  &*&  I0 == append(L0, cons(Pair(k,v), R0))  &*&  Cons(L0, e0, ?L1);
  ensures  I1 == append(L1, cons(Pair(k,v), R0))  &*&  Cons(L0, e0, L1);
{
  open Cons(I0, e0, I1);
  open Cons(L0, e0, L1);
  close Cons(L0, e0, L1);
}
// </removeMin_left_is_removeMin_root>
@*/

#endif
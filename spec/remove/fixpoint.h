#ifndef SPEC_REMOVE_FIXPOINT_H
#define SPEC_REMOVE_FIXPOINT_H

#include "../index.h"

/*@
// <index_remove>
fixpoint list<Pair<int,V> > index_remove<V>(int k, list<Pair<int,V> > I0) {
  switch (I0) {
    case nil: return nil;
    case cons(e0, I1):
      return switch (e0) {
        case Pair(k0, v0):
          return k == k0 ? index_remove(k, I1) : cons(e0, index_remove(k, I1));
      };
  }
}
// </index_remove>
@*/

#endif
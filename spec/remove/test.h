#ifndef TEST_SEQ_REMOVE_H
#define TEST_SEQ_REMOVE_H

#include "fixpoint.h"

/*@
lemma void test_index_remove()
  requires emp;
  ensures emp;
{
  // Remove from empty
  assert index_remove(5, nil) == nil;
  
  // Remove single
  assert
    index_remove(5, cons(Pair(5,4), nil))
    ==                            nil;
  
  // Remove absent
  assert
    index_remove(5, cons(Pair(4,0), cons(Pair(6,6), nil)))
    ==            cons(Pair(4,0), cons(Pair(6,6), nil));
  
  // Remove middle
  assert
    index_remove(5, cons(Pair(4,0), cons(Pair(5,10), cons(Pair(6,2), nil))))
    ==            cons(Pair(4,0),                  cons(Pair(6,2), nil));
  
  // Remove end
  assert
    index_remove(8, cons(Pair(6,9), cons(Pair(8,1), nil)))
    ==            cons(Pair(6,9),                 nil);
}
@*/

#endif
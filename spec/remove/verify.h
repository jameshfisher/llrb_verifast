#ifndef VERIFY_LIST_REMOVE_H
#define VERIFY_LIST_REMOVE_H

#include "../search/fixpoint.h"
#include "./fixpoint.h"

/*@
// <search_after_remove_is_Nothing>
lemma void index_search_after_remove_is_Nothing<V>(int k, list<Pair<int,V> > L0)
  requires true;
  ensures index_search(k, index_remove(k, L0)) == Nothing;
{
  switch (L0) {
    case nil:
    case cons(l0, L1):
      switch (l0) {
        case Pair(k0, v0): index_search_after_remove_is_Nothing(k, L1);
      }
  }
}
// </search_after_remove_is_Nothing>

// <search_unaffected_by_remove_non_search_key>
lemma void index_search_unaffected_by_remove_non_search_key<V>(int k, list<Pair<int,V> > L0, int kn)
  requires kn != k;
  ensures index_search(kn, index_remove(k, L0)) == index_search(kn, L0);
{
  switch (L0) {
    case nil:
    case cons(l0, L1):
      switch (l0) {
        case Pair(k0, v0): index_search_unaffected_by_remove_non_search_key(k, L1, kn);
      }
  }
}
// </search_unaffected_by_remove_non_search_key>

// <index_remove_proof_declaration>
lemma void index_index_remove_proof<V>(int k, list<Pair<int,V> > L0, int kn);
  requires true;
  ensures index_search(kn, index_remove(k, L0)) == (kn == k ? Nothing : index_search(kn, L0));
// </index_remove_proof_declaration>

// <index_remove_proof>
lemma void index_remove_proof<V>(int k, list<Pair<int,V> > L0, int kn)
  requires true;
  ensures index_search(kn, index_remove(k, L0)) == (kn == k ? Nothing : index_search(kn, L0));
{
  if (kn == k) {
    index_search_after_remove_is_Nothing(k, L0);
  }
  else {
    index_search_unaffected_by_remove_non_search_key(k, L0, kn);
  }
}
// </index_remove_proof>
@*/

#endif

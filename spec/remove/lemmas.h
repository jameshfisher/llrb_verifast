#ifndef REMOVE_LEMMAS_H
#define REMOVE_LEMMAS_H

#include "../index.h"
#include "../lemmas.h"
#include "fixpoint.h"

/*@

lemma void remove_preserves_lowerBound<V>(int k, int lb, list<Pair<int,V> > I0)
  requires lowerBound(lb, I0) == true  &&  sorted(I0) == true;
  ensures  lowerBound(lb, index_remove(k, I0)) == true;
{
  switch (I0) {
    case nil:
    case cons(e0, I1):
      switch (e0) {
        case Pair(k0, v0): remove_preserves_lowerBound(k, lb, I1);
      }
  }
}

lemma void remove_preserves_sorted<V>(int k, list<Pair<int,V> > I0)
  requires sorted(I0) == true;
  ensures sorted(index_remove(k, I0)) == true;
{
  switch (I0) {
    case nil:
    case cons(e0, I1):
      switch (e0) {
        case Pair(k0, v0):
          remove_preserves_sorted(k, I1);
          if (k != k0) remove_preserves_lowerBound(k, k0, I1);
      }
  }
}

lemma void remove_lb_is_same<V>(int k, list<Pair<int,V> > I0)
  requires lowerBound(k, I0) == true;
  ensures index_remove(k, I0) == I0;
{
  switch (I0) {
    case nil:
    case cons(e0, I1):
      switch (e0) {
        case Pair(k0, v0): remove_lb_is_same(k, I1);
      }
  }
}

lemma void remove_left<V>(int key, list<Pair<int,V> > L0, int k, V v, list<Pair<int,V> > R0)
  requires key < k  &*&  sorted(append(L0, cons(Pair(k, v), R0))) == true;
  ensures index_remove(key, append(L0, cons(Pair(k,v), R0))) == append(index_remove(key, L0), cons(Pair(k,v), R0));
{
  switch (L0) {
    case nil:
      right_side_of_sorted_is_bound(L0, k, v, R0);
      loosen_lowerBound(key, k, R0);
      remove_lb_is_same(key, cons(Pair(k,v), R0));
    case cons(l0, L1):
      switch (l0) {
        case Pair(k0,v0): remove_left(key, L1, k, v, R0);
      }
  }
}

lemma void remove_right<V>(int key, list<Pair<int,V> > L0, int k, V v, list<Pair<int,V> > R0)
  requires k < key  &*&  sorted(append(L0, cons(Pair(k,v), R0))) == true;
  ensures index_remove(key, append(L0, cons(Pair(k,v), R0))) == append(L0, cons(Pair(k,v), index_remove(key, R0)));
{
  switch (L0) {
    case nil:
    case cons(l0, L1):
      switch (l0) {
        case Pair(k0,v0):
          left_side_of_sorted_is_bound(L0, k, v, R0);
          remove_right(key, L1, k, v, R0);
      }
  }
}

// isn't this the same
lemma void remove_here<V>(int key, list<Pair<int,V> > L0, int k, V v, list<Pair<int,V> > R0)
  requires k < key  &*&  sorted(append(L0, cons(Pair(k,v), R0))) == true;
  ensures index_remove(key, append(L0, cons(Pair(k,v), R0))) == append(L0, cons(Pair(k,v), index_remove(key, R0)));
{
  switch (L0) {
    case nil:
    case cons(l0, L1):
      switch (l0) {
        case Pair(k0, v0):
          left_side_of_sorted_is_bound(L0, k, v, R0);
          remove_here(key, L1, k, v, R0);
      }
  }
}

lemma void remove_root<V>(list<Pair<int,V> > L0, int k, V v, list<Pair<int,V> > R0)
  requires sorted(append(L0, cons(Pair(k,v), R0))) == true;
  ensures index_remove(k, append(L0, cons(Pair(k,v), R0))) == append(L0, R0);
{
  switch (L0) {
    case nil: remove_lb_is_same(k, R0);
    case cons(l0, L1):
      switch (l0) {
        case Pair(k0, v0):
          left_side_of_sorted_is_bound(L0, k, v, R0);
          remove_root(L1, k, v, R0);
      }
  }
}

@*/

#endif

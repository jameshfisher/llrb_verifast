#ifndef BASIC_DATATYPES_H
#define BASIC_DATATYPES_H

/*@
// Basic inductive datatypes

// <List>
inductive List<T> =
  | Nil
  | Cons(T, List<T>);
// </List>

// <Maybe>
inductive Maybe<T> =
  | Nothing
  | Just(T);
// </Maybe>

// <Either>
inductive Either<T1,T2> =
  | Left(T1)
  | Right(T2);
// </Either>

// <Pair>
inductive Pair<K,V> = Pair(K,V);
// </Pair>

// <Cons_predicate>
predicate Cons<T>(list<T> I0, T e0, list<T> I1) =
  switch (I0) {
    case nil: return false;
    case cons(e0', I1'): return e0 == e0' &*& I1 == I1';
  };
// </Cons_predicate>

// <Pair_predicate>
predicate Pair<K,V>(Pair<K,V> p, K k, V v) =
  switch (p) {
    case Pair(k', v'):
      return k == k' &*& v == v';
  };
// </Pair_predicate>

// <IsMaybe_predicate>
predicate IsMaybe<T>(bool is_just, T v, Maybe<T> m) =
  switch (m) {
    case Nothing: return is_just == false;
    case Just(v'): return is_just &*& v == v';
  };
// </IsMaybe_predicate>

// <IsEither_predicate>
predicate IsEither<T>(bool is_right, T v1, T v2, Either<T,T> e) =
  switch (e) {
    case Left(v1'): return is_right == false &*& v1' == v1;
    case Right(v2'): return is_right &*& v2' == v2;
  };
// </IsEither_predicate>
@*/

#endif

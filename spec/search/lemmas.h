#ifndef SPEC_SEARCH_LEMMAS_H
#define SPEC_SEARCH_LEMMAS_H

#include "../index.h"
#include "./fixpoint.h"

#include "../lemmas.h"

/*
// <lb_not_mem_multi_level>
lemma void lb_not_mem<V>(int lb, list<Pair<int,V> > I0)
  requires lowerBound(lb, I0) == true;
  ensures  index_search(lb, I0) == Nothing;
{
  switch (I0) {
    case nil:
    case cons(Pair(k0, v0), I1): lb_not_mem(lb, I1);
  }
}
// </lb_not_mem_multi_level>
*/

/*@

// <lb_not_mem>
lemma void lb_not_mem<V>(int lb, list<Pair<int,V> > I0)
  requires lowerBound(lb, I0) == true;
  ensures  index_search(lb, I0) == Nothing;
{
  switch (I0) {
    case nil:
    case cons(e0, I1):
      switch (e0) {
        case Pair(k0, v0): lb_not_mem(lb, I1);
      }
  }
}
// </lb_not_mem>

// <hb_not_mem>
lemma void hb_not_mem<V>(list<Pair<int,V> > I0, int hb)
  requires higherBound(I0, hb) == true;
  ensures  index_search(hb, I0) == Nothing;
{
  switch (I0) {
    case nil:
    case cons(e0, I1):
      switch (e0) {
        case Pair(k0, v0): hb_not_mem(I1, hb);
      }
  }
}
// </hb_not_mem>

// <lt_lb_not_mem>
lemma void lt_lb_not_mem<V>(int lt_lb, int lb, list<Pair<int,V> > I0)
  requires lt_lb < lb  &&  lowerBound(lb, I0);
  ensures  index_search(lt_lb, I0) == Nothing;
{
  loosen_lowerBound(lt_lb, lb, I0);
  lb_not_mem(lt_lb, I0);
}
// </lt_lb_not_mem>

// <gt_hb_not_mem>
lemma void gt_hb_not_mem<V>(list<Pair<int,V> > I0, int hb, int gt_hb)
  requires hb < gt_hb  &&  higherBound(I0, hb);
  ensures  index_search(gt_hb, I0) == Nothing;
{
  loosen_higherBound(I0, hb, gt_hb);
  hb_not_mem(I0, gt_hb);
}
// </gt_hb_not_mem>

// <not_in_right_list_look_in_left_list>
lemma void not_in_right_list_look_in_left_list<V>(
    int needle,
    list<Pair<int,V> > L0,
    list<Pair<int,V> > R0
    )
  requires index_search(needle, R0) == Nothing;
  ensures  index_search(needle, L0) == index_search(needle, append(L0, R0));
{
  switch (L0) {
    case nil:
    case cons(l0, L1):
      switch (l0) {
        case Pair(k0, v0):
          if (k0 != needle) not_in_right_list_look_in_left_list(needle, L1, R0);
      }
  }
}
// </not_in_right_list_look_in_left_list>

// <not_in_left_list_look_in_right_list>
lemma void not_in_left_list_look_in_right_list<V>(
    int needle,
    list<Pair<int,V> > L0,
    list<Pair<int,V> > R0
    )
  requires index_search(needle, L0) == Nothing;
  ensures  index_search(needle, R0) == index_search(needle, append(L0, R0));
{
  switch (L0) {
    case nil:
    case cons(l0, L1):
      switch (l0) {
        case Pair(k0, v0): not_in_left_list_look_in_right_list(needle, L1, R0);
      }
  }
}
// </not_in_left_list_look_in_right_list>


// <mem_gte_leftmost>
lemma void mem_gte_leftmost<V>(int k0, V v0, list<Pair<int,V> > I1, int kn, V vn)
  requires sorted(cons(Pair(k0,v0), I1)) == true  &*&  index_search(kn, cons(Pair(k0,v0), I1)) == Just(vn);
  ensures  k0 <= kn;
{
  switch (I1) {
    case nil:
    case cons(e1, I2):
      switch (e1) {
        case Pair(k1, v1):
          if (k0 != kn) {
            mem_gte_leftmost(k1, v1, I2, kn, vn);
          }
      }
  }
}
// </mem_gte_leftmost>

// Ugh
// <in_right_list_hb_left_list>
lemma void in_right_list_hb_left_list<V>(int needle, V v, list<Pair<int,V> > L0, list<Pair<int,V> > R0)
  requires sorted(append(L0, R0)) == true  &&  index_search(needle, R0) == Just(v);
  ensures  higherBound(L0, needle) == true;
{
  switch (L0) {
    case nil:
    case cons(l0, L1):
      switch (l0) {
        case Pair(k0, v0):
          in_right_list_hb_left_list(needle, v, L1, R0);
          sides_of_sorted_are_sorted<V>(L0, R0);
          switch (R0) {
            case nil: // not possible
            case cons(r0, R1):
              switch (r0) {
                case Pair(rk0, rv0):
                  left_side_of_sorted_is_bound(L0, rk0, rv0, R1);
                  mem_gte_leftmost(rk0, rv0, R1, needle, v);
                  loosen_higherBound(L0, rk0, needle);
              }
          }
      }
  }
}
// </in_right_list_hb_left_list>

// <in_right_in_append>
lemma void in_right_in_append<V>(int needle, V v, list<Pair<int,V> > L0, list<Pair<int,V> > R0)
  requires sorted(append(L0, R0)) == true  &&  index_search(needle, R0) == Just(v);
  ensures  index_search(needle, append(L0, R0)) == Just(v);
{
  switch (L0) {
    case nil:
    case cons(l0, L1):
      in_right_list_hb_left_list(needle, v, L0, R0);
      hb_not_mem(L0, needle);
      not_in_left_list_look_in_right_list(needle, L0, R0);
  }
}
// </in_right_in_append>

// <in_left_in_append>
lemma_auto void in_left_in_append<V>(int needle, V v, list<Pair<int,V> > L0, list<Pair<int,V> > R0)
  requires index_search(needle, L0) == Just(v);
  ensures  index_search(needle, append(L0, R0)) == Just(v);
{
  switch (L0) {
    case nil:
    case cons(l0, L1):
      switch (l0) {
        case Pair(k0, v0): if (needle != k0) in_left_in_append(needle, v, L1, R0);
      }
  }
}
// </in_left_in_append>

// <root_is_mem>
lemma void root_is_mem<V>(list<Pair<int,V> > L0, int k, V v, list<Pair<int,V> > R0)
  requires sorted(append(L0, cons(Pair(k,v), R0))) == true;
  ensures  index_search(k, append(L0, cons(Pair(k,v), R0))) == Just(v);
{
  in_right_in_append(k, v, L0, cons(Pair(k,v), R0));
}
// </root_is_mem>

@*/

#endif
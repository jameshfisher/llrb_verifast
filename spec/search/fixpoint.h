#ifndef SPEC_SEARCH_FIXPOINT_H
#define SPEC_SEARCH_FIXPOINT_H

#include "../index.h"

/*@
// <index_search>
fixpoint Maybe<V> index_search<K,V>(K needle, list<Pair<K,V> > I0) {
  switch (I0) {
    case nil: return Nothing;
    case cons(kv, I1):
      return switch (kv) {
        case Pair(k, v):
          return k == needle ? Just(v) : index_search(needle, I1);
      };
  }
}
// </index_search>
@*/

#endif
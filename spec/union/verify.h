#ifndef INDEX_UNION_VERIFY_H
#define INDEX_UNION_VERIFY_H

#include "./fixpoint.h"
#include "../search/fixpoint.h"

/*@
// <either>
fixpoint Maybe<T> either<T>(Maybe<T> m0, Maybe<T> m1)
{
  switch (m0) {
    case Just(t): return Just(t);
    case Nothing: return m1;
  }
}
// </either>

// <index_union_proof>
lemma void index_union_proof<K,V>(K kn, list<Pair<K,V> > I0, list<Pair<K,V> > I1)
  requires true;
  ensures index_search(kn, index_union_fixpoint(I0, I1)) == either(index_search(kn, I0), index_search(kn, I1));
{
}
// </index_union_proof>
@*/

#endif
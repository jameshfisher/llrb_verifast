#ifndef SPEC_UNION_SEQ_UNION_H
#define SPEC_UNION_SEQ_UNION_H

#include "../index.h"

/*@
// <index_union_fixpoint>
fixpoint list<Pair<int,V> > index_union_fixpoint<V>(list<Pair<int,V> > IA, list<Pair<int,V> > IB)
{
  switch (IA) {
    case nil: return IB;
    case cons(akv0, IA1):
      return switch (akv0) {
        case Pair(ak0, av0):
          return switch (IB) {
            case nil: return nil;
            case cons(bkv0, IB1):
              return switch (bkv0) {
                case Pair(bk0, bv0):
                  return ak0 <= bk0
                    ? cons(akv0, index_union_fixpoint(IA1, IB))
                    : cons(bkv0, index_union_fixpoint(IB1, IA))
                    ;
              };
          };
      };
  }
}
// </index_union_fixpoint>
@*/

#endif
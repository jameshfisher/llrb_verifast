#ifndef LIST_LEMMAS_H
#define LIST_LEMMAS_H

#include "./index.h"
#include "./fixpoints.h"

/*@

// <lowerBound_implies_keyNotIn_contract>
lemma void lowerBound_implies_keyNotIn<V>(int k, list<Pair<int,V> > I0);
  requires lowerBound(k, I0) == true;
  ensures  keyNotIn(k, I0) == true;
// </lowerBound_implies_keyNotIn_contract>

// <lowerBound_implies_keyNotIn>
lemma void lowerBound_implies_keyNotIn<V>(int k, list<Pair<int,V> > I0)
  requires lowerBound(k, I0) == true;
  ensures  keyNotIn(k, I0) == true;
{
  switch (I0) {
    case nil:
    case cons(e0, I1):
      switch (e0) {
        case Pair(k0, v0):
          lowerBound_implies_keyNotIn(k, I1);
      }
  }
}
// </lowerBound_implies_keyNotIn>

// <sorted_implies_uniqueKeys>
lemma void sorted_implies_uniqueKeys<V>(list<Pair<int,V> > I0)
  requires sorted(I0) == true;
  ensures  uniqueKeys(I0) == true;
{
  switch (I0) {
    case nil:
    case cons(e0, I1):
      switch (e0) {
        case Pair(k0, v0):
          lowerBound_implies_keyNotIn(k0, I1);
          sorted_implies_uniqueKeys(I1);
      }
  }
}
// </sorted_implies_uniqueKeys>

// <loosen_lowerBound>
lemma void loosen_lowerBound<V>(int lt, int lb, list<Pair<int,V> > S)
  requires lt <= lb  &*&  lowerBound(lb, S) == true;
  ensures  lowerBound(lt, S) == true;
{
  switch (S) {
    case nil:
    case cons(kv, I1):
      switch (kv) {
        case Pair(k,v): loosen_lowerBound(lt, lb, I1);
      }
  }
}
// </loosen_lowerBound>

lemma void loosen_higherBound<V>(list<Pair<int,V> > S, int hb, int ht)
  requires higherBound(S, hb) == true  &*&  hb <= ht;
  ensures  higherBound(S, ht) == true;
{
  switch (S) {
    case nil:
    case cons(kv, I1): switch (kv) {
      case Pair(k,v): loosen_higherBound(I1, hb, ht);
    }
  }
}

// <leftmost_is_lower_bound_of_left_list>
lemma void leftmost_is_lower_bound_of_left_list<V>(int k0, V v0, list<Pair<int,V> > L1, list<Pair<int,V> > R0)
  requires sorted(append(cons(Pair(k0, v0), L1), R0)) == true;
  ensures  lowerBound(k0, L1) == true;
{
  switch (L1) {
    case nil:
    case cons(l1, L2):
      switch (l1) {
        case Pair(k1, v1): leftmost_is_lower_bound_of_left_list(k0, v0, L2, R0);
      }
  }
}
// </leftmost_is_lower_bound_of_left_list>

// <sides_of_sorted_are_sorted>
lemma void sides_of_sorted_are_sorted<V>(list<Pair<int,V> > L0, list<Pair<int,V> > R0)
  requires sorted(append(L0, R0)) == true;
  ensures  sorted(L0) == true  &&  sorted(R0) == true;
{
  switch (L0) {
    case nil:
    case cons(l0, L1):
      switch (l0) {
        case Pair(k0, v0):
          leftmost_is_lower_bound_of_left_list(k0, v0, L1, R0);
          sides_of_sorted_are_sorted(L1, R0);
      }
  }
}
// </sides_of_sorted_are_sorted>

// <right_side_of_sorted_is_bound>
lemma void right_side_of_sorted_is_bound<V>(list<Pair<int,V> > L0, int k, V v, list<Pair<int,V> > R0)
  requires sorted(append(L0, cons(Pair(k,v), R0))) == true;
  ensures  lowerBound(k, R0) == true;
{
  sides_of_sorted_are_sorted(L0, cons(Pair(k,v), R0));
}
// </right_side_of_sorted_is_bound>

// <leftmost_less_than_other_member>
lemma void leftmost_less_than_other_member<V>(int k0, V v0, list<Pair<int,V> > L0, int k, V v, list<Pair<int,V> > R0)
  requires sorted(append(cons(Pair(k0,v0), L0), cons(Pair(k,v), R0))) == true;
  ensures  k0 < k;
{
  switch (L0) {
    case nil:
    case cons(l0, L1):
      switch (l0) {
        case Pair(k1, v1): leftmost_less_than_other_member(k1, v1, L1, k, v, R0);
      }
  }
}
// </leftmost_less_than_other_member>

// <left_side_of_sorted_is_bound>
lemma void left_side_of_sorted_is_bound<V>(list<Pair<int,V> > L0, int k, V v, list<Pair<int,V> > R0)
  requires sorted(append(L0, cons(Pair(k,v), R0))) == true;
  ensures  higherBound(L0, k) == true;
{
  switch (L0) {
    case nil:
    case cons(l0, L1):
      switch (l0) {
        case Pair(k0, v0):
          leftmost_less_than_other_member(k0, v0, L1, k, v, R0);
          left_side_of_sorted_is_bound(L1, k, v, R0);
      }
  }
}
// </left_side_of_sorted_is_bound>

// <lowerBound_sides_is_lowerBound_whole>
lemma void lowerBound_sides_is_lowerBound_whole<V>(int lb, list<Pair<int,V> > L0, int k, V v, list<Pair<int,V> > r)
  requires lowerBound(lb, L0) == true &*& lowerBound(lb, r) == true &*& lb < k;
  ensures lowerBound(lb, append(L0, cons(Pair(k,v), r))) == true;
{
  switch (L0) {
    case nil:
    case cons(l0, L1):
      switch (l0) {
        case Pair(k0,v0): lowerBound_sides_is_lowerBound_whole(lb, L1, k, v, r);
      }
  }
}
// </lowerBound_sides_is_lowerBound_whole>

// <higherBound_sides_is_higherBound_whole>
lemma void higherBound_sides_is_higherBound_whole<V>(list<Pair<int,V> > L0, int k, V v, list<Pair<int,V> > r, int hb)
  requires higherBound(L0, hb) == true && higherBound(r, hb) == true && k < hb;
  ensures higherBound(append(L0, cons(Pair(k,v), r)), hb) == true;
{
  switch (L0) {
    case nil:
    case cons(l0, L1):
      switch (l0) {
        case Pair(k0, v0): higherBound_sides_is_higherBound_whole(L1, k, v, r, hb);
      }
  }
}
// </higherBound_sides_is_higherBound_whole>

// <lowerBound_is_lowerBound_right>
lemma void lowerBound_is_lowerBound_right<V>(int lb, list<Pair<int,V> > L0, list<Pair<int,V> > R0)
  requires lowerBound(lb, append(L0, R0)) == true;
  ensures lowerBound(lb, R0) == true;
{
  switch (L0) {
    case nil:
    case cons(l0, L1):
      switch (l0) {
        case Pair(k0,v0): lowerBound_is_lowerBound_right(lb, L1, R0);
      }
  }
}
// </lowerBound_is_lowerBound_right>

// <sorted_sides_sorted_whole>
lemma void sorted_sides_sorted_whole<V>(list<Pair<int,V> > L0, int k, V v, list<Pair<int,V> > r)
  requires sorted(L0) == true && sorted(r) == true && higherBound(L0, k) == true && lowerBound(k, r) == true;
  ensures sorted(append(L0, cons(Pair(k,v), r))) == true;
{
  switch (L0) {
    case nil:
    case cons(l0, L1):
      switch (l0) {
        case Pair(k0,v0):
          sorted_sides_sorted_whole(L1, k, v, r);
          loosen_lowerBound(k0, k, r);
          lowerBound_sides_is_lowerBound_whole(k0, L1, k, v, r);
      }
  }
}
// </sorted_sides_sorted_whole>
@*/

#endif
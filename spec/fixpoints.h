#ifndef SPEC_FIXPOINTS_H
#define SPEC_FIXPOINTS_H

#include "./index.h"

/*@

// <keyNotIn>
fixpoint bool keyNotIn<K,V>(K k, list<Pair<K,V> > I0) {
  switch (I0) {
    case nil: return true;
    case cons(e0,I1):
      return switch (e0) {
        case Pair(k0,v0): return
          k != k0  &&
          keyNotIn(k, I1);
      };
  }
}
// </keyNotIn>

// <uniqueKeys>
fixpoint bool uniqueKeys<K,V>(list<Pair<K,V> > I0) {
  switch (I0) {
    case nil: return true;
    case cons(e0, I1):
      return switch (e0) {
        case Pair(k0,v0): return
          uniqueKeys(I1) &&
          keyNotIn(k0, I1);
      };
  }
}
// </uniqueKeys>

// <lowerBound>
fixpoint bool lowerBound<V>(int lb, list<Pair<int,V> > I0) {
  switch (I0) {
    case nil: return true;
    case cons(e0, I1):
      return switch (e0) {
        case Pair(k0, v0): return
          lb < k0  &&
          lowerBound(lb, I1);
      };
  }
}
// </lowerBound>

// <higherBound>
fixpoint bool higherBound<V>(list<Pair<int,V> > I0, int hb) {
  switch (I0) {
    case nil: return true;
    case cons(kv, I1):
      return switch (kv) {
        case Pair(k, v): return k < hb  &&  higherBound(I1, hb);
      };
  }
}
// </higherBound>

// <sorted>
fixpoint bool sorted<V>(list<Pair<int,V> > I0) {
  switch (I0) {
    case nil: return true;
    case cons(e0, I1):
      return switch (e0) {
        case Pair(k0, v0): return
          lowerBound(k0, I1)  &&
          sorted(I1);
      };
  }
}
// </sorted>

// <maybe_map>
fixpoint Maybe<B> maybe_map<A,B>(fixpoint(A,B) f, Maybe<A> m) {
  switch (m) {
    case Nothing: return Nothing;
    case Just(a): return Just(f(a));
  }
}
// </maybe_map>

@*/

#endif
#ifndef LIST__MAPVALUES__VERIFY_H
#define LIST__MAPVALUES__VERIFY_H

#include "../index.h"
#include "../fixpoints.h"
#include "../search/fixpoint.h"
#include "./fixpoint.h"

/*@
// <index_mapValues_proof>
lemma void index_mapValues_proof<K,V1,V2>(K k, list<Pair<K,V1> > I0, fixpoint(V1,V2) f)
  requires true;
  ensures index_search(k, index_mapValues_fixpoint(f, I0)) == maybe_map(f, index_search(k, I0));
{
  switch (I0) {
    case nil:
    case cons(e0, I1):
      switch (e0) {
        case Pair(k0,v0): index_mapValues_proof(k, I1, f);
      }
  }
}
// </index_mapValues_proof>
@*/

#endif
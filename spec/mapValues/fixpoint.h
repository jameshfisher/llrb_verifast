#ifndef SPEC__MAPVALUES__FIXPOINT_H
#define SPEC__MAPVALUES__FIXPOINT_H

#include "../index.h"

/*@
// <index_mapValues_fixpoint>
fixpoint list<Pair<K,V2> > index_mapValues_fixpoint<K,V1,V2>(fixpoint(V1,V2) f, list<Pair<K,V1> > I0) {
  switch (I0) {
    case nil: return nil;
    case cons(e0, I1):
      return switch (e0) {
        case Pair(k0,v0): return cons(Pair(k0, f(v0)), index_mapValues_fixpoint(f, I1));
      };
  }
}
// </index_mapValues_fixpoint>
@*/

#endif
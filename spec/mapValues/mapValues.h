#ifndef SPEC__MAPVALUES_H
#define SPEC__MAPVALUES_H

#include "../index.h"

/*@
predicate foreachKV<K,V>(list<Pair<K,V> > I0, predicate(K k, V v) p) =
  switch (I0) {
    case nil: return true;
    case cons(e0, I1): return
      switch (e0) {
        case Pair(k0,v0): return p(k0,v0) &*& foreachKV(I1, p);
      };
  };

predicate_family map_func_fixpoint(void* func)(int v, int result);

predicate_ctor map_func_fixpoint(iterator * f)() = map_func_post(f)();


@*/

typedef int map_func(int v);
//@ requires emp;
//@ ensures  result == map_func_fixpoint(this)()(v);

#endif
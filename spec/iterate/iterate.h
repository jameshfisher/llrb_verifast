#ifndef SPEC__ITERATE_H
#define SPEC__ITERATE_H

#include "../index.h"

/*@
// <iterator_post>
predicate_family iterator_post(void* func)(Pair<int,int> pair);
// </iterator_post>

// <iterator_post_ctor>
predicate_ctor iterator_post_ctor(iterator * f)(Pair<int,int> pair) = iterator_post(f)(pair);
// </iterator_post_ctor>
@*/

// <iterator_typedef>
typedef void iterator(int k, int v);
//@ requires true;
//@ ensures iterator_post(this)(Pair(k, v));
// </iterator_typedef>

#endif
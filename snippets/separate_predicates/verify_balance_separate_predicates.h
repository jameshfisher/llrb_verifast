#ifndef VERIFY_BALANCE_H
#define VERIFY_BALANCE_H

#include "llrb_datatype.h"
#include "llrb_lemmas.h"

/*@

// <max>
fixpoint int max(int a, int b)
{
  return a < b ? b : a;
}
// </max>

// <min>
fixpoint int min(int a, int b)
{
  return a < b ? a : b;
}
// </min>

// <shortest_path>
fixpoint int shortest_path<K,V>(Tree<K,V> t)
{
  switch (t) {
    case Leaf: return 0;
    case Branch(l, k, v, c, r):
      return min(shortest_path(l), shortest_path(r))+1;
  }
}
// </shortest_path>

// <longest_path>
fixpoint int longest_path<K,V>(Tree<K,V> t)
{
  switch (t) {
    case Leaf: return 0;
    case Branch(l, k, v, c, r):
      return max(longest_path(l), longest_path(r))+1;
  }
}
// </longest_path>


// <NearlyBalanced>
predicate NearlyBalanced<K,V>(Tree<K,V> t) =
  longest_path(t) <= 2 * shortest_path(t);
// </NearlyBalanced>


// The ease of the following is due to the theorem prover.

lemma void llrb_subtrees<K,V>(Tree<K,V> t)
  requires IsLLRB(t, ?c, ?h)  &*&  Branch(t, ?l, ?k, ?v, c, ?r);
  ensures  IsLLRB(l, _, _)  &*&  IsLLRB(r, _, _);
{
  open Branch(t, l, k, v, c, r);
  open IsLLRB(t, c, h);
  open RedRule(t, c);
  open BlackRule(t, h);
  close IsLLRB(l, _, _);
  close IsLLRB(r, _, _);
}

// <llrb_shortest_path>
lemma void llrb_shortest_path<K,V>(Tree<K,V> t)
  requires IsLLRB(t, ?c, ?h);
  ensures  IsLLRB(t,  c,  h)  &*&  shortest_path(t) == (c == Blk ? h : h+1);
{
  switch (t) {
    case Leaf: leaf_is_black_0();
    case Branch(l, k, v, c0, r):
      close Branch(t, l, k, v, c0, r);
      llrb_subtrees(t);
      llrb_shortest_path(l);
      llrb_shortest_path(r);
      close IsLLRB(t, c, h);
      return;
  }
}
// </llrb_shortest_path>

// <llrb_longest_path>
lemma void llrb_longest_path<K,V>(Tree<K,V> t)
  requires IsLLRB(t, ?c, ?h);
  ensures  IsLLRB(t,  c,  h)  &*&  longest_path(t) <= (c == Blk ? 2*h : (2*h)+1);
{
  open IsLLRB(t, c, h);
  switch (t) {
    case Leaf:
    case Branch(l, k, v, c0, r):
      llrb_longest_path(l);
      llrb_longest_path(r);
  }
  close IsLLRB(t, c, h);
}
// </>


// <llrb_nearly_balanced>
lemma void llrb_nearly_balanced<K,V>(Tree<K,V> t)
  requires IsLLRB(t, ?c, ?h);
  ensures  IsLLRB(t,  c,  h)  &*&  NearlyBalanced(t);
{
  llrb_shortest_path(t);
  llrb_longest_path(t);
  close NearlyBalanced(t);
}
// </>

@*/

#endif

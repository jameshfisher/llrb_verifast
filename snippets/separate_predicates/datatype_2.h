#ifndef IsLLRB_DATATYPE_H
#define IsLLRB_DATATYPE_H

#include "../spec/basic_datatypes.h"

/*@
// <RedRule>
predicate RedRule<K,V>(Tree<K,V> t; Color c) =
  switch (t) {
    case Leaf: return c == Blk;
    case Branch(l,k,v,c',r): return
      c == c'  &*&
      RedRule(l, ?lc)  &*&
      RedRule(r, Blk)  &*&
      (c == Red ? lc == Blk : true);
  };
// </RedRule>

// <BlackRule>
predicate BlackRule<K,V>(Tree<K,V> t; int h) =
  switch (t) {
    case Leaf: return h == 0;
    case Branch(l,k,v,c,r): return
      BlackRule(l, ?subh)  &*&
      BlackRule(r,  subh)  &*&
      (c == Red ? h == subh : h == subh+1);
  };
// </BlackRule>

// <IsLLRB_predicate_separate_rules>
predicate IsLLRB'<K,V>(Tree<K,V> t, Color c, int h) = RedRule(t, c)  &*&  BlackRule(t, h);
// </IsLLRB_predicate_separate_rules>
@*/

#endif

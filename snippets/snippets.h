
// <id_nonterminating>
bool id(bool b)
//@ requires emp;
//@ ensures result == b;
{
  while (true)
  //@ invariant emp;
  {
  }
    
  return b;
}
// </id_nonterminating>

// <list_search_nonterminating>
bool list_search_nonterminating(int needle, int * value, ListTreeNode * listTreeNode)
//@ requires IsList(listTreeNode, ?L)  &*&  integer(value, ?v0);
//@ ensures  IsList(listTreeNode,  L)  &*&  integer(value, ?v1)  &*&  IsMaybe(result, v1, index_search(needle, L));
{
  return list_search_nonterminating(needle, value, listTreeNode);
}
// </list_search_nonterminating>

// <factorial_iterative_commented>
// returns n * (n-1) * (n-2) * ... * 2 * 1
int factorial(int n)
{
  // n >= 0
  
  int result = 1;
  int i = 0;
  
  while (i < n) {
    // result == factorial(i)
    i++;
    result = i * result;
  }
  
  return result;
}
// </factorial_iterative_commented>

// <typedef_index>
typedef Index<K,V> = List<Pair<K,V> >;
// </typedef_index>

// <fixpoint_semicolon>
fixpoint bool foo(list<int> l) {
  switch (l) {
    case nil: return true;
    case cons(x,xs): return true;
  };
}
// </fixpoint_semicolon>

// <struct_TreeNode_pres>
struct TreeNode {
  int key;
  int value;
  Color color;
  TreeNode * lft;
  TreeNode * rgt;
};
// </struct_TreeNode_pres>

// <llrb_insert_declarations>
bool llrb_insert_R(int key, int value, TreeNodePtr * rootptr);
bool llrb_insert_B(int key, int value, TreeNodePtr * rootptr);
// </llrb_insert_declarations>

// <llrb_insert_B_contract>
bool llrb_insert_B(int key, int value, TreeNodePtr * rootptr)
//@ requires pointer(rootptr, ?root0) &*& IsTree(root0, ?t0) &*& sorted(flatten(t0)) == true &*& IsLLRB(t0, Blk, ?h);
//@ ensures  pointer(rootptr, ?root1) &*& IsTree(root1, ?t1) &*& IsLLRB(t1, result ? Red : Blk, h)  &*&  flatten(t1) == index_insert(key, value, flatten(t0));
// </llrb_insert_B_contract>
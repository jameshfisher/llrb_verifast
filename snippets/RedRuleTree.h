#ifndef REDRULETREE_H
#define REDRULETREE_H

/*@
// <RedRuleTree>
inductive LLRBTree<K,V> =
  | Leaf
  | Black1(RedTree<K,V>,  K, V, LLRBTree<K,V>)
  | Black2(LLRBTree<K,V>, K, V, LLRBTree<K,V>)
  ;

inductive RedTree<K,V> =
  | Red(LLRBTree<K,V>, K, V, LLRBTree<K,V>)
  ;
// </RedRuleTree>

// <RedRuleTree_BlackHeight>
predicate BlackHeightB<K,V>(LLRBTree<K,V> t, int h) =
  switch (t) {
    case Leaf: return h == 0;
    case Black1(l,k,v,r): return BlackHeightR(l, h-1) &*& BlackHeightB(r, h-1);
    case Black2(l,k,v,r): return BlackHeightB(l, h-1) &*& BlackHeightB(r, h-1);
  };

predicate BlackHeightR<K,V>(RedTree<K,V> t, int h) =
  switch (t) {
    case Red(l,k,v,r): return BlackHeightB(l, h) &*& BlackHeightB(r, h);
  };
// </RedRuleTree_BlackHeight>

@*/

// <returnsFive>
int returnsFive()
//@ requires 1 == 2;
//@ ensures result == 5;
{
  return 4;
}
// </returnsFive>

#endif
#ifndef INSERT_BST_FIXPOINT_H
#define INSERT_BST_FIXPOINT_H

#include "../../../llrb/impl/spec/tree.h"
#include "../../../../spec/index.h"

/*@
// <bst_insert_fixpoint>
fixpoint Tree<int,V> bst_insert_fixpoint<K,V>(int nk, V nv, Tree<int,V> t) {
  switch (t) {
    case Leaf: return Branch(Leaf, nk, nv, Red, Leaf);
    case Branch(l, k, v, c, r):
      return
        nk < k ? Branch(bst_insert_fixpoint(nk, nv, l), k, v, c, r) :
        k < nk ? Branch(l, k, v, c, bst_insert_fixpoint(nk,nv,r)) :
                 Branch(l, k, nv, c, r)
        ;
  }
}
// </bst_insert_fixpoint>

@*/

#endif
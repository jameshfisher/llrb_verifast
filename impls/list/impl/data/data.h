#ifndef IMPLS__LIST_IMPL__DATA__DATA_H
#define IMPLS__LIST_IMPL__DATA__DATA_H

#include "../../../../spec/index.h"

// <struct_ListNode>
struct ListNode;
typedef struct ListNode ListNode; 

struct ListNode {
  int        key;
  int        value;
  ListNode * tail;
};
// </struct_ListNode>


/*@
// <IsListNode>
predicate IsListNode(ListNode * node; int key, int value, ListNode * tail) =
  node->key   |-> key    &*&
  node->value |-> value  &*&
  node->tail  |-> tail   &*&
  malloc_block_ListNode(node)
  ;
// </IsListNode>

// <IsListSeg_declaration>
predicate IsListSeg(ListNode * in, ListNode * out; list<Pair<int,int> > L);
// </IsListSeg_declaration>

// <IsListSeg>
predicate IsListSeg(ListNode * in, ListNode * out; list<Pair<int,int> > L) =
  in == out ?
    L == nil
  :
    IsListNode(in, ?key, ?value, ?tail) &*&
    IsListSeg(tail, out, ?L1)           &*&
    L == cons(Pair(key, value), L1)
  ;
// </IsListSeg>

// <IsList>
predicate IsList(ListNode * head; list<Pair<int,int> > L) = IsListSeg(head, 0, L);
// </IsList>

// <IsCircularList>
predicate IsCircularList(ListNode * head; list<Pair<int,int> > L) =
  head == 0 ?
    L == nil
  :
    IsListNode(head, ?key, ?value, ?tail) &*&
    IsListSeg(tail, head, ?L1)            &*&
    L == cons(Pair(key, value), L1)
  ;
// </IsCircularList>
@*/

#endif
#ifndef IMPLS__LIST__IMPL__OPS__ITERATE__ITERATE_H
#define IMPLS__LIST__IMPL__OPS__ITERATE__ITERATE_H

#include "../../../../../spec/mapValues/mapValues.h"
#include "../../data/data.h"

void list_mapValues(map_func * f, ListNode * root);
//@ requires IsList(root, ?I0)  &*&  is_map_func(f) == true;
//@ ensures  IsList(root,  index_mapValues(map_func_fixpoint(f), I0));

#endif
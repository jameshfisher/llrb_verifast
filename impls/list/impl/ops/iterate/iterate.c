#include "./iterate.h"

void list_iterate(iterator * it, ListNode * root)
//@ requires is_iterator(it) == true  &*&  IsList(root, ?I0);
//@ ensures  is_iterator(it) == true  &*&  IsList(root,  I0)  &*&  foreachKV(I0, iterator_post_ctor(it));
{
  //@ open IsList(root, I0);
  //@ open IsListSeg(root, 0, I0);
  
  if (root != 0) {
    it(root->key, root->value);
    list_iterate(it, root->tail);
    //@ close iterator_post_ctor(it)(root->key, root->value);
  }
  
  //@ close foreachKV(I0, iterator_post_ctor(it));
  //@ close IsListSeg(root, 0, I0);
  //@ close IsList(root, I0);
}
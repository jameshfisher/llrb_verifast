#ifndef IMPLS__LIST__IMPL__OPS__ITERATE__ITERATE_H
#define IMPLS__LIST__IMPL__OPS__ITERATE__ITERATE_H

#include "../../../../../spec/iterate/iterate.h"
#include "../../data/data.h"

void list_iterate(iterator * it, ListNode * root);
//@ requires is_iterator(it) == true  &*&  IsList(root, ?I0);
//@ ensures  is_iterator(it) == true  &*&  IsList(root,  I0)  &*&  foreachKV(I0, iterator_post_ctor(it));

#endif
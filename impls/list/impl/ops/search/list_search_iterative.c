#include "./list_search.h"

// <list_search_iterative>
bool list_search_iterative(int needle, int * value, ListNode * listNode)
//@ requires IsList(listNode, ?I)  &*&  integer(value, ?v0);
//@ ensures  IsList(listNode,  I)  &*&  integer(value, ?v1)  &*&  IsMaybe(result, v1, index_search(needle, I));
{
  ListNode * i = listNode;
  
  while (i != 0)
  //@ invariant IsListSeg(listNode, i, ?I0)  &*&  integer(value, v0)  &*& IsList(i, ?I1) &*& I == append(I0, I1) &*& index_search(needle, I0) == Nothing;
  {
    //@ open IsListSeg(listNode, i, I0);
    if (listNode->key == needle) {
      *value = listNode->value;
      //@ close IsMaybe(true, listNode->value, index_search(needle, I));
      return true;
    }
    else {
      i = i->tail;
    }
  }
    
  return false;
}
// </list_search_iterative>

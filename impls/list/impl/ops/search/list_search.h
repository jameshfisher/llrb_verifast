#ifndef SEARCH_SEARCH_LIST_H
#define SEARCH_SEARCH_LIST_H

#include "../../data/data.h"
#include "../../../../../spec/search/fixpoint.h"

// <list_search_declaration>
bool list_search(int needle, int * value, ListNode * listNode);
//@ requires IsList(listNode, ?L)  &*&  integer(value, ?v0);
//@ ensures  IsList(listNode,  L)  &*&  integer(value, ?v1)  &*&  IsMaybe(result, v1, index_search(needle, L));
// </list_search_declaration>

#endif
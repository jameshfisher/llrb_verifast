#include "./list_search.h"

// <list_search>
bool list_search(int needle, int * value, ListNode * listNode)
//@ requires IsList(listNode, ?I)  &*&  integer(value, ?v0);
//@ ensures  IsList(listNode,  I)  &*&  integer(value, ?v1)  &*&  IsMaybe(result, v1, index_search(needle, I));
{
  //@ open IsList(listNode, I);
  //@ open IsListSeg(listNode, 0, I);
  
  if (listNode == 0) {
    //@ close IsMaybe(false, v0, index_search(needle, I));
    return false;
  }
  else {
    if (listNode->key == needle) {
      *value = listNode->value;
      //@ close IsMaybe(true, listNode->value, index_search(needle, I));
      return true;
    }
    else {
      return list_search(needle, value, listNode->tail);
    }
  }
}
// </list_search>

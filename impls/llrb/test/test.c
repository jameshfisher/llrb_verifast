#include "stdio.h"

#include "../IsLLRB/IsLLRB.h"

#define MAX_VALUE 20


void print(char * cs)
//@ requires emp;
//@ ensures  emp;
{
  fputs(cs, stdout);
}

void print_insert(int key, int value, IsLLRB tree)
//@ requires IsLLRBTree(tree, ?S0);
//@ ensures  IsLLRBTree(tree, ?S1);
{
  print("insert(");
  printf("%i", key);
  print(",");
  printf("%i", value);
  print(")\n");
  IsLLRB_insert(key, value, tree);
}

void print_remove(int needle, IsLLRB tree)
//@ requires IsLLRBTree(tree, ?S0);
//@ ensures  IsLLRBTree(tree, ?S1);
{
  print("remove(");
  printf("%i", needle);
  print(")\n");
  IsLLRB_remove(needle, tree);
}

void print_contents(IsLLRB tree)
//@ requires IsLLRBTree(tree, ?S0);
//@ ensures  IsLLRBTree(tree,  S0);
{
  print("Contents: {  ");

  int i;
  for (i = 0; i < MAX_VALUE; i++)
  //@ invariant IsLLRBTree(tree, S0);
  {
    int value;
    bool in = IsLLRB_search(i, &value, tree);
    //@ open IsMaybe(in, value, _);
    if (in) {
      printf("%i", i);
      print(":");
      printf("%i", value);
      print("  ");
    }
  }

  puts("}");
}

int main()
//@ requires emp;
//@ ensures emp;
{
  IsLLRB tree = IsLLRB_new();
  
  print_contents(tree);

  int ks[10] = {6,4,5,8,9,0,1,3,2,7};
  int vs[10] = {3,7,2,4,4,0,9,8,7,1};

  int i;
  for (i = 0; i < 10; i++)
  //@ invariant IsLLRBTree(tree, _);
  {
    print_insert(ks[i], vs[i], tree);
    print_contents(tree);
  }
  
  for (i = 0; i < 10; i++)
  //@ invariant IsLLRBTree(tree, _);
  {
    print_remove(ks[i], tree);
    print_contents(tree);
  }
  
  IsLLRB_dispose(tree);
  
  return 0;
}
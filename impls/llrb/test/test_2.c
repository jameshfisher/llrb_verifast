#include "stdlib.h"
#include "stdio.h"

#include "../IsLLRB/IsLLRB.h"


int main()
//@ requires emp;
//@ ensures emp;
{
  bool r;
  int v;
  
  IsLLRB tree = IsLLRB_new();
  //@ assert IsLLRBTree(tree, nil);
  
  IsLLRB_insert(5, 6, tree);
  //@ assert IsLLRBTree(tree, cons(Pair(5,6), nil));

  IsLLRB_insert(7, 3, tree);
  //@ assert IsLLRBTree(tree, cons(Pair(5,6), cons(Pair(7,3), nil)));

  IsLLRB_insert(-6, 0, tree);
  //@ assert IsLLRBTree(tree, cons(Pair(-6,0), cons(Pair(5,6), cons(Pair(7,3), nil))));

  IsLLRB_insert(5, 2, tree);
  //@ assert IsLLRBTree(tree, cons(Pair(-6,0), cons(Pair(5,2), cons(Pair(7,3), nil))));
  
  r = IsLLRB_search(45, &v, tree);
  //@ open IsMaybe(_, _, _);
  //@ assert r == false;

  r = IsLLRB_search(7, &v, tree);
  //@ open IsMaybe(_, _, _);
  //@ assert r == true;
  //@ assert v == 3;

    
  IsLLRB_remove(5, tree);
  //@ assert IsLLRBTree(tree, cons(Pair(-6,0), cons(Pair(7,3), nil)));

  IsLLRB_remove(456, tree);
  //@ assert IsLLRBTree(tree, cons(Pair(-6,0), cons(Pair(7,3), nil)));
  
  IsLLRB_remove(-6, tree);
  //@ assert IsLLRBTree(tree, cons(Pair(7,3), nil));
  
  IsLLRB_dispose(tree);
  
  return 0;
}

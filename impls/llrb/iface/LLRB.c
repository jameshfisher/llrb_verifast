#include <stdlib.h>

#include "IsLLRB.h"

#include "../dispose/llrb_dispose.h"
#include "../search/llrb_search.h"
#include "../insert/insert.h"
#include "../remove/remove.h"

#include "../tree_lemmas/tree_search_lemmas.h"

IsLLRB IsLLRB_new()
//@ requires emp;
//@ ensures  IsLLRBTree(result, nil);
{
  IsLLRB result = malloc(sizeof(struct IsLLRB));
  if (result == 0) {
    abort();
  }
  else {
    result->root = 0;
    //@ close Tree(0, Leaf);
    //@ close IsLLRB(Leaf, Blk, 0);
    //@ close IsLLRBTree(result, nil);
    return result;
  }
}


void IsLLRB_dispose(IsLLRB tree)
//@ requires IsLLRBTree(tree, _);
//@ ensures  emp;
{
  //@ open IsLLRBTree(tree, _);
  //@ assert tree->root |-> ?root  &*&  Tree(root, ?t);
  //@ llrb_emp(t);
  llrb_dispose(tree->root);
  free(tree);
}

bool IsLLRB_search(int key, int * value, IsLLRB tree)
//@ requires IsLLRBTree(tree, ?S0)  &*&  integer(value,   _);
//@ ensures  IsLLRBTree(tree,  S0)  &*&  integer(value, ?v1)  &*&  IsMaybe(result, v1, index_search(key, S0));
{
  //@ open IsLLRBTree(tree, S0);
  //@ assert tree->root |-> ?root  &*&  Tree(root, ?t);
  bool result = llrb_search(key, value, tree->root);
  //@ close IsLLRBTree(tree, S0);
  //@ tree_search_is_list_search(key, t);
  return result;
}

void IsLLRB_insert(int key, int value, IsLLRB tree)
//@ requires IsLLRBTree(tree,                       ?S0 );
//@ ensures  IsLLRBTree(tree, index_insert(key, value, S0));
{
  //@ open IsLLRBTree(tree, S0);
  ///@ assert tree->root |-> ?root &*& Tree(root, ?t);
  ///@ bst_is_sorted(t);
  llrb_insert(key, value, &(tree->root));
  ///@ index_insert_preserves_sorted(key, value, S0);
  //@ close IsLLRBTree(tree, index_insert(key, value, S0));
}

void IsLLRB_remove(int key, IsLLRB tree)
//@ requires IsLLRBTree(tree,                ?S0 );
//@ ensures  IsLLRBTree(tree, index_remove(key, S0));
{
  //@ open IsLLRBTree(tree, S0);
  llrb_remove_B(&(tree->root), key);
  ///@ remove_preserves_sorted(key, S0);
  //@ close IsLLRBTree(tree, index_remove(key, S0));
}

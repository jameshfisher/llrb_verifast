#ifndef IsLLRB_H
#define IsLLRB_H

#include "../spec/index.h"
#include "../spec/search/index_search.h"
#include "../spec/insert/index_insert.h"
#include "../spec/remove/index_remove.h"
#include "../data/TreeNode.h"
#include "../tree_lemmas/tree_fixpoints.h"

struct IsLLRB;
typedef struct IsLLRB * IsLLRB;

struct IsLLRB {
  TreeNodePtr root;
};

/*@
predicate IsLLRBTree(IsLLRB tree, list<Pair<int,int> > sequence)
  =    tree->root |-> ?root
  &*&  malloc_block_IsLLRB(tree)
  &*&  Tree(root, ?t)
  &*&  bst(t) == true
  &*&  IsLLRB(t, Blk, _)
  &*&  sequence == flatten(t)
  ;
@*/

IsLLRB IsLLRB_new();
//@ requires emp;
//@ ensures  IsLLRBTree(result, nil);

void IsLLRB_dispose(IsLLRB tree);
//@ requires IsLLRBTree(tree, _);
//@ ensures  emp;

bool IsLLRB_search(int key, int * value, IsLLRB tree);
//@ requires IsLLRBTree(tree, ?S0)  &*&  integer(value,   _);
//@ ensures  IsLLRBTree(tree,  S0)  &*&  integer(value, ?v1)  &*&  IsMaybe(result, v1, index_search(key, S0));

void IsLLRB_insert(int key, int value, IsLLRB tree);
//@ requires IsLLRBTree(tree,                       ?S0 );
//@ ensures  IsLLRBTree(tree, index_insert(key, value, S0));

void IsLLRB_remove(int key, IsLLRB tree);
//@ requires IsLLRBTree(tree,                ?S0 );
//@ ensures  IsLLRBTree(tree, index_remove(key, S0));

#endif  // IsLLRB_H
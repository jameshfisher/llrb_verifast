#ifndef NODE_H
#define NODE_H

#include "../spec/tree.h"
#include "../spec/ctx/ctx.h"

// <struct_TreeNode>
struct TreeNode;
typedef struct TreeNode * TreeNodePtr; 

struct TreeNode {
  int key;
  int value;
  Color color;
  TreeNodePtr lft;
  TreeNodePtr rgt;
};
// </struct_TreeNode>

/*@
// <IsTreeNode>
predicate IsTreeNode(TreeNodePtr node; TreeNodePtr l, int k, int v, Color c, TreeNodePtr r)
  =    node != 0
  &*&  node->lft    |->  l
  &*&  node->rgt    |->  r
  &*&  node->key    |->  k
  &*&  node->value  |->  v
  &*&  node->color  |->  c
  &*&  malloc_block_TreeNode(node)
  ;
// </IsTreeNode>

// <IsTree_predicate>
predicate IsTree(TreeNodePtr root; Tree<int,int> tree) =
  root == 0 ?
    tree == Leaf
  :
    root->key    |->  ?key    &*&
    root->value  |->  ?value  &*&
    root->color  |->  ?color  &*&
    root->lft    |->  ?lft    &*&
    root->rgt    |->  ?rgt    &*&
    malloc_block_TreeNode(root)   &*&
    IsTree(lft, ?lftTree)     &*&
    IsTree(rgt, ?rgtTree)     &*&
    tree == Branch(lftTree, key, value, color, rgtTree)
  ;
// </IsTree_predicate>

// <IsCtx_predicate>
predicate IsCtx(TreeNodePtr root, TreeNodePtr tree, Ctx<int,int> ctx)
  = switch (ctx) {
      case NilCtx: return root == tree;
      case LftCtx(lft, k, v, c, pctx):
        return IsTreeNode(?node, ?lftptr, k, v, c, tree)
          &*&  IsTree(lftptr, lft)
          &*&  IsCtx(root, node, pctx)
          ;
      case RgtCtx(k, v, c, rgt, pctx):
        return IsTreeNode(?node, tree, k, v, c, ?rgtptr)
          &*&  IsTree(rgtptr, rgt)
          &*&  IsCtx(root, node, pctx)
          ;
    };
// </IsCtx_predicate>
@*/


#endif  //NODE_H

#ifndef NODE_LEMMAS_H
#define NODE_LEMMAS_H

#include "./node.h"
#include "../spec/ctx/fixpoints.h"

/*@
lemma void null_ptr_is_Leaf(TreeNodePtr root)
  requires IsTree(root, ?t)  &*&  root == 0;
  ensures  IsTree(root,  t)  &*&  t == Leaf;
{
  open IsTree(root, t);
  close IsTree(root, t);
}

lemma void rebuild_tree(TreeNodePtr root, TreeNodePtr i, Tree<int,int> t)
  requires  IsCtx(root, i, ?ctx)  &*&  IsTree(i, ?subt)  &*& ctx_compose(ctx, subt) == t;
  ensures   IsTree(root, t);
{
  open IsCtx(root, i, ctx);
  switch (ctx) {
    case NilCtx:
    case LftCtx(lft, k, v, c, pctx):
      open IsTreeNode(?node, ?l, k, v, c, i);
      close IsTree(node, Branch(lft, k, v, c, subt));
      rebuild_tree(root, node, t);
    case RgtCtx(k, v, c, rgt, pctx):
      open IsTreeNode(?node, i, k, v, c, ?r);
      close IsTree(node, Branch(subt, k, v, c, rgt));
      rebuild_tree(root, node, t);
  }
}
@*/

#endif
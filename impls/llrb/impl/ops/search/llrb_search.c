#include "./llrb_search.h"

#include "../../spec/lemmas.h"
#include "../../../../../spec/search/lemmas.h"

/*
// <llrb_search_recursive_unverified>
bool llrb_search_recursive(int needle, int * value, TreeNodePtr root) {
  if (root == 0) {
    return false;
  }
  else {
    int key = root->key;
    bool result;
    if (needle < key) {
      result = llrb_search_recursive(needle, value, root->lft);
    }
    else if (key < needle) {
      result = llrb_search_recursive(needle, value, root->rgt);
    }
    else {
      result = true;
      *value = root->value;
    }
    return result;
  }
}
// </llrb_search_recursive_unverified>
*/

// <llrb_search_recursive>
bool llrb_search_recursive(int needle, int * value, TreeNodePtr root)
//@ requires IsTree(root, ?t)  &*&  integer(value, ?v0)  &*&  bst(t) == true;
//@ ensures  IsTree(root,  t)  &*&  integer(value, ?v1)  &*&  IsMaybe(result, v1, tree_search(needle, t));
{
  //@ open IsTree(root, t);
  if (root == 0) {
    //@ close IsTree(root, t);
    //@ close IsMaybe(false, v0, tree_search(needle, Leaf));
    return false;
  }
  else {
    //@ assert root->lft |-> ?lft  &*&  IsTree(lft, ?l)  &*&  root->rgt |-> ?rgt  &*&  IsTree(rgt, ?r);
    int key = root->key;
    bool result;
    if (needle < key) {
      result = llrb_search_recursive(needle, value, root->lft);
      //@ open IsMaybe(result, ?v1, tree_search(needle, l));
      //@ close IsMaybe(result, v1, tree_search(needle, t));
    }
    else if (key < needle) {
      result = llrb_search_recursive(needle, value, root->rgt);
      //@ open IsMaybe(result, ?v1, tree_search(needle, r));
      //@ close IsMaybe(result, v1, tree_search(needle, t));
    }
    else {
      result = true;
      *value = root->value;
      //@ close IsMaybe(result, root->value, tree_search(needle, t));
    }
    //@ close IsTree(root, t);
    return result;
  }
}
// </llrb_search_recursive>

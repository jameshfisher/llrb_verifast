#ifndef SEARCH_FIXPOINT_H
#define SEARCH_FIXPOINT_H

#include "../../../../../spec/index.h"
#include "../../spec/bst.h"

/*@
// <tree_search>
fixpoint Maybe<V> tree_search<V>(int needle, Tree<int,V> t)
{
  switch (t) {
    case Leaf: return Nothing;
    case Branch(l, k, v, c, r):
      return
        needle < k ?
          tree_search(needle, l)
        : k < needle ?
          tree_search(needle, r)
        :
          Just(v);
  }
}
// </tree_search>
@*/

#endif
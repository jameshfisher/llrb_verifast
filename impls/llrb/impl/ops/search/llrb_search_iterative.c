#include "./llrb_search.h"

#include "../../spec/ctx/ctx.h"
#include "../../spec/ctx/lemmas.h"
#include "../../data/lemmas.h"

/*
// <llrb_search_iterative_unverified>
bool llrb_search(int needle, int * value, TreeNodePtr root)
{
  TreeNodePtr i = root;
  
  while (i != 0) {
    int k = i->key;

    if (needle < k) {
      i = i->lft;
    }
    else if (k < needle) {
      i = i->rgt;
    }
    else {
      *value = i->value;
      return true;
    }
  }
  return false;
}
// </llrb_search_iterative_unverified>
*/

// <llrb_search_iterative>
bool llrb_search(int needle, int * value, TreeNodePtr root)
//@ requires IsTree(root, ?t)  &*&  integer(value, ?v0)  &*&  bst(t) == true;
//@ ensures  IsTree(root,  t)  &*&  integer(value, ?v1)  &*&  IsMaybe(result, v1, tree_search(needle, t));
{
  TreeNodePtr i = root;
  
  //@ close IsCtx(root, i, NilCtx);
  
  while (i != 0)
  //@ invariant IsCtx(root, i, ?ctx)  &*&  IsTree(i, ?subt)  &*& ctx_compose(ctx, subt) == t  &*&  tree_search(needle, t) == tree_search(needle, subt)  &*& integer(value, v0);
  {
    //@ open IsTree(i, subt);
    //@ assert i->lft |-> ?lft &*& i->rgt |-> ?rgt &*& IsTree(lft, ?l0) &*& IsTree(rgt, ?r0) &*& i->value |-> ?v &*& i->color |-> ?clr;

    int k = i->key;

    TreeNodePtr prev_i = i; // ugly

    if (needle < k) {
      i = i->lft;
      //@ close IsTreeNode(prev_i, lft, k, v, clr, rgt);
      //@ close IsCtx(root, lft, RgtCtx(k, v, clr, r0, ctx));
    }
    else if (k < needle) {
      i = i->rgt;
      //@ close IsTreeNode(prev_i, lft, k, v, clr, rgt);
      //@ close IsCtx(root, rgt, LftCtx(l0, k, v, clr, ctx));
    }
    else {
      *value = i->value;
      //@ rebuild_tree(root, i, t);
      //@ close IsMaybe(true, v, tree_search(needle, subt));
      return true;
    }
  }
  //@ null_ptr_is_Leaf(i);
  //@ rebuild_tree(root, i, t);
  //@ close IsMaybe(false, v0, tree_search(needle, Leaf));
  return false;
}
// </llrb_search_iterative>
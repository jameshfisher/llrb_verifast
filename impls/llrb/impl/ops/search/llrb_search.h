#ifndef SEARCH_H
#define SEARCH_H

#include "../../../../../spec/index.h"
#include "./fixpoint.h"
#include "../../data/node.h"

// <llrb_search_declaration>
bool llrb_search(int needle, int * value, TreeNodePtr root);
//@ requires IsTree(root, ?t)  &*&  integer(value, ?v0)  &*&  bst(t) == true;
//@ ensures  IsTree(root,  t)  &*&  integer(value, ?v1)  &*&  IsMaybe(result, v1, tree_search(needle, t));
// </llrb_search_declaration>

#endif
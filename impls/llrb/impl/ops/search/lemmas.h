#ifndef TREE_SEARCH_LEMMAS_H
#define TREE_SEARCH_LEMMAS_H

#include "./fixpoint.h"
#include "../spec/index.h"
#include "../bst_spec/lemmas.h"
#include "../spec/search/lemmas.h"


/*@
// <tree_search_is_list_search_declaration>
lemma void tree_search_is_list_search<V>(int needle, Tree<int,V> t);
  requires bst(t) == true;
  ensures  tree_search(needle, t) == index_search(needle, flatten(t));
// </tree_search_is_list_search_declaration>

      
// <tree_search_is_list_search>
lemma void tree_search_is_list_search<V>(int needle, Tree<int,V> t)
  requires bst(t) == true;
  ensures  tree_search(needle, t) == index_search(needle, flatten(t));
{
  switch (t) {
    case Leaf:
    case Branch(l, k, v, c, r):
      bst_is_sorted(t);
      sides_of_sorted_are_sorted(flatten(l), cons(Pair(k, v), flatten(r)));
      
      if (needle < k) {
        lt_lb_not_mem(needle, k, flatten(r));
        not_in_right_list_look_in_left_list(needle, flatten(l), cons(Pair(k,v), flatten(r)));
        tree_search_is_list_search(needle, l);
      }
      else if (k < needle) {
        left_side_of_sorted_is_bound(flatten(l), k, v, flatten(r));
        gt_hb_not_mem(flatten(l), k, needle);
        not_in_left_list_look_in_right_list(needle, flatten(l), cons(Pair(k,v), flatten(r)));
        tree_search_is_list_search(needle, r);
      }
      else {
        in_right_in_append(k, v, flatten(l), cons(Pair(k,v), flatten(r)));
      }
  }
}
// </tree_search_is_list_search>
@*/

#endif
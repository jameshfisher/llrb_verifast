#include "./removeRoot.h"
#include "../removeMin/removeMin.h"
#include "../../spec/balance/node_lemmas.h"
#include "../../spec/balance/lemmas.h"
#include "../freeTreeNode.h"
#include "../../fix/has_black_root/has_black_root.h"
#include "../../fix/has_black_root/lemmas.h"
#include "../../fix/blacken_R/blacken_R.h"
#include "../../fix/blacken_R/lemmas.h"
#include "../../fix/fix_short_right_B/fix_short_right_B.h"

// <llrb_removeRoot_B>
bool llrb_removeRoot_B(TreeNodePtr * rootptr)
//@ requires pointer(rootptr, ?root0)  &*&  IsTree(root0, ?t0)  &*&  IsLLRB(t0, Blk, ?h0)  &*&  0 < h0      &*&  Branch(t0, ?l0, _, _, Blk, ?r0);
//@ ensures  pointer(rootptr, ?root1)  &*&  IsTree(root1, ?t1)  &*&  IsLLRB(t1, Blk, (result ? h0-1 : h0))  &*&  flatten(t1) == append(flatten(l0), flatten(r0));
{
  TreeNodePtr root = *rootptr;
  
  //@ open IsLLRB(t0, Blk, h0);
  //@ open IsTree(root, t0);
  //@ open Branch(t0, l0, _, _, Blk, r0);
  
  if (root->rgt == 0) {
    //@ null_ptr_is_black_Leaf(root->rgt);
    //@ black_0_has_no_elements(r0);
    
    //@ dispose_null_ptr(root->rgt);
    
    TreeNodePtr lft = root->lft;
    *rootptr = root->lft;
    
    freeTreeNode(root);
    
    ///@ assert IsTree(lft, ?l0);
    //@ has_black_root_LLRB(l0);
    if (has_black_root(lft)) {
      return true;
    }
    else {
      //@ red_has_branch(l0);
      //@ blacken_R_applied_to_R(l0);
      blacken_R(lft);
      return false;
    }
  }
  else {
    //@ black_non_null_has_height(root->rgt);
    //@ black_with_height_has_elements(r0);
    //@ open  Cons(flatten(r0), ?rmin, ?rrest);
    //@ close Cons(flatten(r0),  rmin,  rrest);
    //@ close Pair(rmin, _, _);
    bool fix = removeMinB(&(root->rgt), &(root->key), &(root->value));
      
    //@ assert IsTree(root, ?t1);
     
    if (fix) {
      //@ close Branch(t1, _, _, _, Blk, _);
      return fix_short_right_B(rootptr);
    }
    else {
      //@ close IsLLRB(t1, Blk, h0);
      return false;
    }
  }
}
// </llrb_removeRoot_B>
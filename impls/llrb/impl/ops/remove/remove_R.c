#include "./remove.h"
#include "./removeRoot.h"

#include "../../fix/fix_short_left_R/fix_short_left_R.h"
#include "../../fix/fix_short_right_R/fix_short_right_R.h"
#include "../../../../../spec/lemmas.h"
#include "../../../../../spec/remove/lemmas.h"

// <maybe_fix_left>
bool maybe_fix_left(TreeNodePtr * rootptr, bool fix)
//@ requires pointer(rootptr, ?root0)  &*&  IsTree(root0, ?t0)  &*&  Branch(t0, ?l0, _, _, Red, ?r0)  &*&  IsLLRB(l0, Blk, ?lh)  &*&  IsLLRB(r0, Blk, ?rh)  &*&  (fix ? rh == lh + 1 : rh == lh);
//@ ensures  pointer(rootptr, ?root1)  &*&  IsTree(root1, ?t1)  &*&  IsLLRB(t1, (result ? Blk : Red), rh)  &*&  flatten(t1) == flatten(t0);
{
  TreeNodePtr root = *rootptr; 
  if (fix) {
    return fix_short_left_R(rootptr);
  }
  else {
    //@ open Branch(t0, l0, _, _, Red, r0);
    //@ close IsLLRB(t0, Red, rh);
    return false;
  }
}
// </maybe_fix_left>

// <llrb_remove_R>
bool llrb_remove_R(TreeNodePtr * rootptr, int key)
//@ requires pointer(rootptr, ?root0)  &*&  IsTree(root0, ?t0)  &*&  sorted(flatten(t0)) == true  &*&  IsLLRB(t0,                 Red,  ?h0);
//@ ensures  pointer(rootptr, ?root1)  &*&  IsTree(root1, ?t1)  &*&  sorted(flatten(t1)) == true  &*&  IsLLRB(t1, (result ? Blk : Red),  h0)  &*&  flatten(t1) == index_remove(key, flatten(t0));
{
  TreeNodePtr root = *rootptr;
  
  //@ open IsTree(root, t0);
  //@ open IsLLRB(t0, Red, h0);

  int k = root->key;
  
  //@ assert root->lft |-> ?lft  &*&  IsTree(lft, ?l0)  &*&  root->rgt |-> ?rgt  &*&  IsTree(rgt, ?r0)  &*&  root->value |-> ?v;
  
  //@ sides_of_sorted_are_sorted(flatten(l0), cons(Pair(k,v), flatten(r0)));
  //@ remove_preserves_sorted(key, flatten(t0));
  
  if (key < k) {
    //@ remove_left(key, flatten(l0), k, v, flatten(r0));
    bool fix = llrb_remove_B(&(root->lft), key);
    //@ assert IsTree(root, ?t1);
    //@ close Branch(t1, ?l1, k, _, Red, r0);
    
    return maybe_fix_left(rootptr, fix);
  }
  
  else if (k < key) {
    //@ remove_right(key, flatten(l0), k, v, flatten(r0));
    bool fix = llrb_remove_B(&(root->rgt), key);
    //@ assert IsTree(root, ?t1);
    if (fix) {
      //@ close Branch(t1, _, _, _, _, _);
      return fix_short_right_R(rootptr);
    }
    else {
      //@ close IsLLRB(t1, Red, h0);
      //@ close IsTree(root, t1);
      return false;
    }
  }
  
  else {
    //@ close IsLLRB(t0, Red, h0);
    //@ close Branch(t0, l0, k, v, Red, r0);
    //@ remove_root(flatten(l0), k, v, flatten(r0));
    return llrb_removeRoot_R(rootptr);
  }
}
// </llrb_remove_R>
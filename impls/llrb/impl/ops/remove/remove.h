#ifndef REMOVE_H
#define REMOVE_H

#include "../../data/node.h"
#include "../../spec/balance/predicate.h"
#include "../../spec/flatten.h"
#include "../../../../../spec/fixpoints.h"
#include "../../../../../spec/remove/fixpoint.h"

// <llrb_remove_R_declaration>
bool llrb_remove_R(TreeNodePtr * rootptr, int key);
//@ requires pointer(rootptr, ?root0)  &*&  IsTree(root0, ?t0)  &*&  sorted(flatten(t0)) == true  &*&  IsLLRB(t0,                 Red,  ?h0);
//@ ensures  pointer(rootptr, ?root1)  &*&  IsTree(root1, ?t1)  &*&  sorted(flatten(t1)) == true  &*&  IsLLRB(t1, (result ? Blk : Red),  h0)  &*&  flatten(t1) == index_remove(key, flatten(t0));
// </llrb_remove_R_declaration>

// <llrb_remove_B_declaration>
bool llrb_remove_B(TreeNodePtr * rootptr, int key);
//@ requires pointer(rootptr, ?root0)  &*&  IsTree(root0, ?t0)  &*&  sorted(flatten(t0)) == true  &*&  IsLLRB(t0, Blk,                 ?h0);
//@ ensures  pointer(rootptr, ?root1)  &*&  IsTree(root1, ?t1)  &*&  sorted(flatten(t1)) == true  &*&  IsLLRB(t1, Blk, (result ? h0-1 : h0))  &*&  flatten(t1) == index_remove(key, flatten(t0));
// </llrb_remove_B_declaration>

#endif  // REMOVE_H
#include "./removeRoot.h"
#include "./remove.h"
#include "../../fix/fix_short_right_R/fix_short_right_R.h"
#include "../removeMin/removeMin.h"
#include "../../spec/balance/node_lemmas.h"
#include "../freeTreeNode.h"

// <llrb_removeRoot_R>
bool llrb_removeRoot_R(TreeNodePtr * rootptr)
//@ requires pointer(rootptr, ?root0)  &*&  IsTree(root0, ?t0)  &*&  IsLLRB(t0,                 Red,  ?h0)  &*&  Branch(t0, ?l0, _, _, Red, ?r0);
//@ ensures  pointer(rootptr, ?root1)  &*&  IsTree(root1, ?t1)  &*&  IsLLRB(t1, (result ? Blk : Red),  h0)  &*&  flatten(t1) == append(flatten(l0), flatten(r0));
{
  TreeNodePtr root = *rootptr;
  
  //@ open IsLLRB(t0, Red, h0);
  //@ open IsTree(root, t0);
  //@ open Branch(t0, l0, _, _, Red, r0);
  
  if (root->rgt == 0) {
    //@ null_ptr_is_black_Leaf(root->rgt);
    //@ black_0_has_no_elements(r0);
    
    //@ dispose_null_ptr(root->rgt);
    
    TreeNodePtr lft = root->lft;
    *rootptr = root->lft;
    
    freeTreeNode(root);
    
    return true;
  }
  else {
    //@ black_non_null_has_height(root->rgt);
    //@ black_with_height_has_elements(r0);
    //@ open  Cons(flatten(r0), ?rmin, ?rrest);
    //@ close Cons(flatten(r0),  rmin,  rrest);
    //@ close Pair(rmin, _, _);
    bool fix = removeMinB(&(root->rgt), &(root->key), &(root->value));
      
    //@ assert IsTree(root, ?t1);
     
    if (fix) {
      //@ close Branch(t1, _, _, _, Red, _);
      return fix_short_right_R(rootptr);
    }
    else {
      //@ close IsLLRB(t1, Red, h0);
      return false;
    }
  }
}
// </llrb_removeRoot_R>
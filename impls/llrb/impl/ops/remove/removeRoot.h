#ifndef DO_REMOVEMIN_H
#define DO_REMOVEMIN_H

#include "../../data/node.h"
#include "../../spec/balance/predicate.h"
#include "../../spec/flatten.h"

/*@
//predicate Append<T>(list<T> S0, list<T> L0, T e, list<T> R0) = S0 == append<T>(L0, cons(e, R0));
@*/

// <llrb_removeRoot_B_declaration>
bool llrb_removeRoot_B(TreeNodePtr * rootptr);
//@ requires pointer(rootptr, ?root0)  &*&  IsTree(root0, ?t0)  &*&  IsLLRB(t0, Blk, ?h0)  &*&  0 < h0      &*&  Branch(t0, ?l0, _, _, Blk, ?r0);
//@ ensures  pointer(rootptr, ?root1)  &*&  IsTree(root1, ?t1)  &*&  IsLLRB(t1, Blk, (result ? h0-1 : h0))  &*&  flatten(t1) == append(flatten(l0), flatten(r0));
// </llrb_removeRoot_B_declaration>

// <llrb_removeRoot_R_declaration>
bool llrb_removeRoot_R(TreeNodePtr * rootptr);
//@ requires pointer(rootptr, ?root0)  &*&  IsTree(root0, ?t0)  &*&  IsLLRB(t0,                 Red,  ?h0)  &*&  Branch(t0, ?l0, _, _, Red, ?r0);
//@ ensures  pointer(rootptr, ?root1)  &*&  IsTree(root1, ?t1)  &*&  IsLLRB(t1, (result ? Blk : Red),  h0)  &*&  flatten(t1) == append(flatten(l0), flatten(r0));
// </llrb_removeRoot_R_declaration>

#endif
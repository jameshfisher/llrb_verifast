#include "./remove.h"
#include "./removeRoot.h"

#include "../../spec/balance/node_lemmas.h"
#include "../../../../../spec/lemmas.h"
#include "../../../../../spec/remove/lemmas.h"
#include "../../fix/has_black_root/has_black_root.h"
#include "../../fix/has_black_root/lemmas.h"
#include "../../fix/fix_short_left_B/fix_short_left_B.h"
#include "../../fix/fix_short_right_B/fix_short_right_B.h"

// <llrb_remove_B>
bool llrb_remove_B(TreeNodePtr * rootptr, int key)
//@ requires pointer(rootptr, ?root0)  &*&  IsTree(root0, ?t0)  &*&  sorted(flatten(t0)) == true  &*&  IsLLRB(t0, Blk,                 ?h0);
//@ ensures  pointer(rootptr, ?root1)  &*&  IsTree(root1, ?t1)  &*&  sorted(flatten(t1)) == true  &*&  IsLLRB(t1, Blk, (result ? h0-1 : h0))  &*&  flatten(t1) == index_remove(key, flatten(t0));
{
  TreeNodePtr root = *rootptr;
  if (root == 0) {
    //@ null_ptr_is_black_Leaf(root);
    //@ black_0_has_no_elements(t0);
    return false;
  }
  else {
    //@ open IsTree(root, t0);
    int k = root->key;
    
    //@ assert root->lft |-> ?lft  &*&  IsTree(lft, ?l0)  &*&  root->rgt |-> ?rgt  &*&  IsTree(rgt, ?r0)  &*&  root->value |-> ?v;
    
    //@ sides_of_sorted_are_sorted(flatten(l0), cons(Pair(k,v), flatten(r0)));
    //@ remove_preserves_sorted(key, flatten(t0));
      
    //@ open IsLLRB(t0, Blk, h0);
    if (key < k) {
      //@ has_black_root_LLRB(l0);
      if (has_black_root(root->lft)) {
        //@ remove_left(key, flatten(l0), k, v, flatten(r0));
        bool fix = llrb_remove_B(&(root->lft), key);
    
        //@ assert IsTree(root, ?t1);
        if (fix) {
          //@ close Branch(t1, _, _, _, Blk, _);
          return fix_short_left_B(rootptr);
        }
        else {
          //@ close IsLLRB(t1, Blk, h0);
          return false;
        }
      }
      else {
        //@ remove_left(key, flatten(l0), k, v, flatten(r0));
        bool fix = llrb_remove_R(&(root->lft), key);
        //@ close IsTree(root, ?t1);
        //@ close Branch(t1, _, _, _, _, _);
        //@ b3_or_b2(t1, fix);
        return false;
      }
    }
    
    else if (key > k) {
      //@ remove_right(key, flatten(l0), k, v, flatten(r0));
      bool fix = llrb_remove_B(&(root->rgt), key);
      //@ close IsTree(root, ?t1);
      if (fix) {
        //@ close Branch(t1, _, _, _, _, _);
        return fix_short_right_B(rootptr);
      }
      else {
        //@ close IsLLRB(t1, Blk, h0);
        return false;
      }
    }
    
    else {
      //@ close IsLLRB(t0, Blk, h0);
      //@ black_non_null_has_height(root);
      //@ close Branch(t0, _, _, _, _, _);
      //@ remove_root(flatten(l0), k, v, flatten(r0));
      return llrb_removeRoot_B(rootptr);
    }
  }
}
// </llrb_remove_B>
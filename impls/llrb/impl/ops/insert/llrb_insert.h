#ifndef LLRB_INSERT_H
#define LLRB_INSERT_H

#include "../../../../../spec/insert/fixpoint.h"
#include "../../../../../spec/fixpoints.h"
#include "../../data/node.h"
#include "../../spec/tree.h"
#include "../../spec/flatten.h"
#include "../../spec/balance/predicate.h"
#include "../../fix/predicates.h"

// <llrb_insert_declaration>
void llrb_insert(int key, int value, TreeNodePtr * rootptr);
//@ requires pointer(rootptr, ?root0) &*& IsTree(root0, ?t0) &*& sorted(flatten(t0)) == true &*& IsLLRB(t0, Blk, _);
//@ ensures  pointer(rootptr, ?root1) &*& IsTree(root1, ?t1) &*& sorted(flatten(t1)) == true &*& IsLLRB(t1, Blk, _)  &*&  flatten(t1) == index_insert(key, value, flatten(t0));
// </llrb_insert_declaration>

#endif
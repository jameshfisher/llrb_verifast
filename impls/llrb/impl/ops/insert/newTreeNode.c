#include "newNode.h"

#include <stdlib.h>

// <newTreeNode>
TreeNodePtr newTreeNode(int key, int value)
//@ requires emp;
//@ ensures IsTree(result, ?t)  &*&  IsLLRB(t, Red, 0)  &*&  flatten(t) == cons(Pair(key, value), nil);
{
  TreeNodePtr result = malloc(sizeof(struct TreeNode));
  if (result == 0) { abort(); }
  result->key = key;
  result->value = value;
  result->lft = 0;
  result->rgt = 0;
  result->color = Red;
  //@ close IsTree(result, Branch(Leaf, key, value, Red, Leaf));
  //@ close IsLLRB(Leaf, Blk, 0);
  //@ close IsLLRB(Leaf, Blk, 0);
  //@ close IsLLRB(Branch(Leaf, key, value, Red, Leaf), Red, 0);
  return result;
}   
// </newTreeNode>
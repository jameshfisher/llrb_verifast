#include "./recursive.h"

#include "../../../../../../spec/insert/lemmas.h"
#include "../../../fix/fix_RVRight/lemma.h"
#include "../../../fix/rotate/single/rotate.h"
#include "../../../fix/rotate/single/lemmas.h"

// <llrb_insert_R>
bool llrb_insert_R(int key, int value, TreeNodePtr * rootptr)
//@ requires pointer(rootptr, ?root0) &*& IsTree(root0, ?t0) &*& sorted(flatten(t0)) == true  &*&  IsLLRB(t0, Red, ?h);
//@ ensures  pointer(rootptr, ?root1) &*& IsTree(root1, ?t1) &*& (result ? RV(t1, h) : IsLLRB(t1, Red, h)) &*& flatten(t1) == index_insert(key, value, flatten(t0));
{
  TreeNodePtr root = *rootptr;
  
  //@ open IsLLRB(t0, Red, h);
  //@ open IsTree(root, t0);
  
  int k = root->key;
  
  //@ assert root->lft |-> ?lft  &*&  IsTree(lft, ?l0)  &*&  root->rgt |-> ?rgt  &*&  IsTree(rgt, ?r0)  &*&  root->value |-> ?v;

  //@ sides_of_sorted_are_sorted(flatten(l0), cons(Pair(k, v), flatten(r0)));
  if (key < k) {
    //@ index_insert_left(key, value, flatten(l0), k, v, flatten(r0));
    
    bool fix = llrb_insert_B(key, value, &(root->lft));
    //@ assert IsTree(root, ?t1);
        
    if (fix) {
      //@ close IsTree(root, t1);
      //@ close Branch(t1, _, _, _, _, _);
      //@ close RV(t1, h);
      return true;
    }
    else {
      //@ close IsTree(root, t1);
      //@ close IsLLRB(t1, Red, h);
      return false;
    }
  }
  
  else if (k < key) {
    //@ index_insert_right(key, value, flatten(l0), k, v, flatten(r0));
    bool fix = llrb_insert_B(key, value, &(root->rgt));
    //@ assert IsTree(root, ?t1);
    //@ assert flatten(t1) == index_insert(key, value, flatten(t0));
    
    if (fix) {
      //@ close IsTree(root, t1);
      //@ close Branch(t1, _, _, _, _, _);
      //@ close RVRight(t1, h);
      //@ RVRight_has_right_branch(t1);
      //@ rotate_left_fixes_RVRight(t1);
      //@ rotate_left_maintains_values(t1);
      root = rotate_left(root);
      *rootptr = root;
      return true;
    }
    else {
      //@ close IsTree(root, t1);
      //@ close IsLLRB(t1, Red, h);
      return false;
    }
  }
  
  else {
    root->value = value;
    //@ index_insert_here(key, value, flatten(l0), k, v, flatten(r0));
    //@ close IsTree(root, ?t1);
    //@ close IsLLRB(t1, Red, h);
    return false;
  }
}
// </llrb_insert_R>

#ifndef LLRB_INSERT_RECURSIVE_H
#define LLRB_INSERT_RECURSIVE_H

#include "../llrb_insert.h"

// <llrb_insert_R_declaration>
bool llrb_insert_R(int key, int value, TreeNodePtr * rootptr);
//@ requires pointer(rootptr, ?root0) &*& IsTree(root0, ?t0) &*& sorted(flatten(t0)) == true  &*&  IsLLRB(t0, Red, ?h);
//@ ensures  pointer(rootptr, ?root1) &*& IsTree(root1, ?t1) &*& (result ? RV(t1, h) : IsLLRB(t1, Red, h)) &*& flatten(t1) == index_insert(key, value, flatten(t0));
// </llrb_insert_R_declaration>

// <llrb_insert_B_declaration>
bool llrb_insert_B(int key, int value, TreeNodePtr * rootptr);
//@ requires pointer(rootptr, ?root0) &*& IsTree(root0, ?t0) &*& sorted(flatten(t0)) == true &*& IsLLRB(t0, Blk, ?h);
//@ ensures  pointer(rootptr, ?root1) &*& IsTree(root1, ?t1) &*& IsLLRB(t1, result ? Red : Blk, h)  &*&  flatten(t1) == index_insert(key, value, flatten(t0));
// </llrb_insert_B_declaration>

#endif
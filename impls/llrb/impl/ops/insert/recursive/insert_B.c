#include "./recursive.h"

#include "../newTreeNode.h"
#include "../../../../../../spec/lemmas.h"
#include "../../../../../../spec/insert/lemmas.h"
#include "../../../fix/has_black_root/has_black_root.h"
#include "../../../fix/has_black_root/lemmas.h"
#include "../../../fix/fix_B3Right/fix_B3Right.h"
#include "../../../fix/fix_B3Right/lemmas.h"
#include "../../../fix/fix_RVLeft/fix_RVLeft.h"
#include "../../../fix/fix_B4/fix_B4.h"

/*
// <llrb_insert_B_unverified>
bool llrb_insert_B(int key, int value, TreeNodePtr * rootptr)
{
  TreeNodePtr root = *rootptr;
  
  if (root == 0) { ... }
  else {
    int k = root->key;
  
    if (key == k) { ... }
    else {
      if (has_black_root(root->lft)) { ... }
      else {
        if (key < k) {
          bool fix = llrb_insert_R(key, value, &(root->lft));
          if (fix) {
            //@ assert RVLeft(t1, h);
            root = fix_RVLeft(root);
            *rootptr = root;
            return true;
          }
          else {
            return false;
          }
        }
        else { ... }
      }
    }
  }
}
// </llrb_insert_B_unverified>
*/

// TODO: re-order comparisons?


// <llrb_insert_B>
bool llrb_insert_B(int key, int value, TreeNodePtr * rootptr)
//@ requires pointer(rootptr, ?root0) &*& IsTree(root0, ?t0) &*& sorted(flatten(t0)) == true &*& IsLLRB(t0, Blk, ?h);
//@ ensures  pointer(rootptr, ?root1) &*& IsTree(root1, ?t1) &*& IsLLRB(t1, result ? Red : Blk, h)  &*&  flatten(t1) == index_insert(key, value, flatten(t0));
{
  TreeNodePtr root = *rootptr;
  
  //@ open IsLLRB(t0, Blk, h);
  //@ open IsTree(root, t0);
  
  if (root == 0) {
    root = newTreeNode(key, value);
    *rootptr = root;
    return true;
  }
  else {  
    int k = root->key;
  
    //@ assert root->lft |-> ?lft  &*&  IsTree(lft, ?l0)  &*&  root->rgt |-> ?rgt  &*&  IsTree(rgt, ?r0)  &*&  root->value |-> ?v;
  
    //@ sides_of_sorted_are_sorted(flatten(l0), cons(Pair(k, v), flatten(r0)));
  
    if (key == k) {
      root->value = value;
      //@ index_insert_here(key, value, flatten(l0), k, v, flatten(r0));
      //@ close IsTree(root, ?t1);
      //@ close IsLLRB(t1, Blk, h);
      return false;
    }
    else {
      //@ has_black_root_LLRB(l0);
      if (has_black_root(root->lft)) {
        if (key < k) {
          //@ index_insert_left(key, value, flatten(l0), k, v, flatten(r0));
          llrb_insert_B(key, value, &(root->lft));
          //@ close IsTree(root, ?t1);
          //@ close IsLLRB(t1, Blk, h);
        }
        else {
          //@ index_insert_right(key, value, flatten(l0), k, v, flatten(r0));
          bool fix = llrb_insert_B(key, value, &(root->rgt));
          //@ assert IsTree(root, ?t1);
          if (fix) {
            //@ close Branch(t1, _, _, _, _, _);
            //@ close B3Right(t1, h);
            root = fix_B3Right(root);
            //@ fix_B3Right_lemma(t1);
            //@ assert IsTree(root, ?t2);
            //@ open B3(t2, h);
            //@ open Branch(t2, _, _, _, _, _);
            *rootptr = root;
          }
          //@ assert IsTree(root, ?t3);
          //@ close IsLLRB(t3, Blk, h);
        }
        return false;
      }
      else {
        if (key < k) {
          //@ index_insert_left(key, value, flatten(l0), k, v, flatten(r0));
          bool fix = llrb_insert_R(key, value, &(root->lft));
          //@ assert IsTree(root, ?t1);
          if (fix) {
            //@ close Branch(t1, _, _, _, _, _);
            //@ close RVLeft(t1, h);
            root = fix_RVLeft(root);
            *rootptr = root;
            return true;
          }
          else {
            //@ close IsLLRB(t1, Blk,  h);
            //@ close IsTree(root, t1);
            return false;
          }
        }
        else {
          //@ index_insert_right(key, value, flatten(l0), k, v, flatten(r0));
          bool fix = llrb_insert_B(key, value, &(root->rgt));
          //@ assert IsTree(root, ?t1);
          if (fix) {
            //@ close Branch(t1, _, _, _, _, _);
            //@ close B4(t1, h);
            fix_B4(root);
            return true;
          }
          else {
            //@ close IsLLRB(t1, Blk,  h);
            //@ close IsTree(root, t1);
            return false;
          }
        }
      }
    }
  }
}
// </llrb_insert_B>
#include "../llrb_insert.h"
//#include "../help/blacken.h"

#include "./recursive.h"
#include "../../../fix/blacken_R/blacken_R.h"
#include "../../../spec/balance/lemmas.h"

// <llrb_insert>
void llrb_insert(int key, int value, TreeNodePtr * rootptr)
//@ requires LLRB_BST(rootptr, ?t0, Blk, _);
//@ ensures  LLRB_BST(rootptr, ?t1, Blk, _)  &*&  flatten(t1) == index_insert(key, value, flatten(t0));
{
  bool red = llrb_insert_B(key, value, rootptr);
  if (red) {
    //@ open LLRB_BST(rootptr, ?t1, Red, ?h);
    //@ open BST(rootptr, t1);
    //@ red_is_branch(t1);
    blacken_R(*rootptr);
  }
}
// </llrb_insert>

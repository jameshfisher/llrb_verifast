#ifndef NEWNODE_H
#define NEWNODE_H

#include "../../data/node.h"
#include "../../spec/balance/predicate.h"
#include "../../spec/flatten.h"

// <newTreeNode_declaration>
TreeNodePtr newTreeNode(int key, int value);
//@ requires emp;
//@ ensures IsTree(result, ?t)  &*&  IsLLRB(t, Red, 0)  &*&  flatten(t) == cons(Pair(key, value), nil);
// </newTreeNode_declaration>

#endif
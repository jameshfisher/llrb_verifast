#include "llrb_dispose.h"

#include "freeTreeNode.h"

void llrb_dispose(TreeNodePtr root)
//@ requires IsTree(root, _);
//@ ensures emp;
{
  //@ open IsTree(root, _);
  if (root != 0) {
    llrb_dispose(root->lft);
    llrb_dispose(root->rgt);
    freeTreeNode(root);
  }
}
#ifndef DISPOSETREE_H
#define DISPOSETREE_H

#include "../../data/node.h"

void llrb_dispose(TreeNodePtr root);
//@ requires IsTree(root, _);
//@ ensures emp;

#endif  // DISPOSETREE_H
#ifndef FREENODE_H
#define FREENODE_H

#include "../data/node.h"

// <freeTreeNode_declaration>
void freeTreeNode(TreeNodePtr node);
//@ requires node->key |-> _  &*&  node->value |-> _  &*&  node->color |-> _  &*&  node->lft |-> _  &*&  node->rgt |-> _  &*&  malloc_block_TreeNode(node);
//@ ensures emp;
// </freeTreeNode_declaration>

#endif  // FREENODE_H
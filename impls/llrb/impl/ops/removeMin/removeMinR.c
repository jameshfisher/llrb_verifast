#include "./removeMin.h"

#include "../../../../../spec/removeMin/lemmas.h"
#include "../../spec/balance/node_lemmas.h"
#include "../freeTreeNode.h"
#include "../../fix/fix_short_left_R/fix_short_left_R.h"

// <removeMinR>
bool removeMinR(TreeNodePtr * rootptr, int * key, int * value)
//@ requires pointer(rootptr, ?root0)  &*&  IsTree(root0, ?t0)  &*&  IsLLRB(t0,                 Red,  ?h0)  &*&  Cons(flatten(t0), ?kv, ?rest)  &*&  Pair(kv, ?k, ?v)  &*&  integer(key, _)  &*&  integer(value, _);
//@ ensures  pointer(rootptr, ?root1)  &*&  IsTree(root1, ?t1)  &*&  IsLLRB(t1, (result ? Blk : Red),  h0)  &*&  flatten(t1) == rest  &*&  integer(key, k)  &*&  integer(value, v);
{
  TreeNodePtr root = *rootptr;
  
  //@ open IsLLRB(t0, Red, h0);
  //@ open IsTree(root, t0);
  //@ open Cons(flatten(t0), kv, rest);
  //@ open Pair(kv, k, v);
  
  //@ assert root->lft |-> ?lft  &*&  IsTree(lft, ?l0)  &*&  root->rgt |-> ?rgt  &*&  IsTree(rgt, ?r0);
    
  if (root->lft == 0) {
    *key = root->key;
    *value = root->value;
    
    //@ null_ptr_is_black_Leaf(root->lft);
    
    //@ black_0_has_no_elements(l0);
    //@ black_0_has_no_elements(r0);
    
    //@ dispose_null_ptr(root->lft);
    //@ dispose_black_0(root->rgt);
    freeTreeNode(root);
    *rootptr = 0;
    //@ close IsTree(0, Leaf);
    //@ close IsLLRB(Leaf, Blk, 0);
    
    return true;
  }
  else {
    //@ black_non_null_has_height(root->lft);
    //@ black_with_height_has_elements(l0);
    
    //@ close Cons(flatten(t0), kv, rest);
    //@ close Pair(kv, k, v);
    //@ leftmost_of_left_is_leftmost_of_whole(flatten(t0), flatten(l0), root->key, root->value, flatten(r0));
    //@ removeMin_left_is_removeMin_root(flatten(t0), flatten(l0), root->key, root->value, flatten(r0));
    
    bool fix = removeMinB(&(root->lft), key, value);
    
    //@ assert IsTree(root, ?t1);
    if (fix) {
      //@ close Branch(t1, _, _, _, Red, _);
      return fix_short_left_R(rootptr);
    }
    else {
      //@ close IsTree(root, t1);
      //@ close IsLLRB(t1, Red, h0);
      return false;
    }
  }
}
// </removeMinR>
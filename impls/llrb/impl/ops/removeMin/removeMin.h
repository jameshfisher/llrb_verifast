#ifndef REMOVEMIN_H
#define REMOVEMIN_H

#include "../../data/node.h"
#include "../../spec/balance/predicate.h"
#include "../../spec/flatten.h"
#include "../../../../../spec/index.h"

// <removeMinR_declaration>
bool removeMinR(TreeNodePtr * rootptr, int * key, int * value);
//@ requires pointer(rootptr, ?root0)  &*&  IsTree(root0, ?t0)  &*&  IsLLRB(t0,                 Red,  ?h0)  &*&  Cons(flatten(t0), ?kv, ?rest)  &*&  Pair(kv, ?k, ?v)  &*&  integer(key, _)  &*&  integer(value, _);
//@ ensures  pointer(rootptr, ?root1)  &*&  IsTree(root1, ?t1)  &*&  IsLLRB(t1, (result ? Blk : Red),  h0)  &*&  flatten(t1) == rest  &*&  integer(key, k)  &*&  integer(value, v);
// </removeMinR_declaration>

// <removeMinB_declaration>
bool removeMinB(TreeNodePtr * rootptr, int * key, int * value);
//@ requires pointer(rootptr, ?root0)  &*&  IsTree(root0, ?t0)  &*&  IsLLRB(t0, Blk,                 ?h0)  &*& 0 < h0  &*&  Cons(flatten(t0), ?kv, ?rest)  &*&  Pair(kv, ?k, ?v)  &*&  integer(key, _)  &*&  integer(value, _);
//@ ensures  pointer(rootptr, ?root1)  &*&  IsTree(root1, ?t1)  &*&  IsLLRB(t1, Blk, (result ? h0-1 : h0))  &*&  flatten(t1) == rest    &*&  integer(key, k)  &*&  integer(value, v);
// </removeMinB_declaration>

#endif  // REMOVEMIN_H
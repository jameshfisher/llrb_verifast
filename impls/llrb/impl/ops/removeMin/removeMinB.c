#include "./removeMin.h"

#include "../../spec/balance/node_lemmas.h"
#include "../freeTreeNode.h"
#include "../../fix/has_black_root/has_black_root.h"
#include "../../fix/has_black_root/lemmas.h"
#include "../../fix/fix_short_left_B/fix_short_left_B.h"

#include "../../../../../spec/removeMin/lemmas.h"

// <removeMinB>
bool removeMinB(TreeNodePtr * rootptr, int * key, int * value)
//@ requires rootptr |-> ?root0  &*&  IsTree(root0, ?t0)  &*&  IsLLRB(t0, Blk,                 ?h0)  &*& 0 < h0  &*&  Cons(flatten(t0), ?kv, ?rest)  &*&  Pair(kv, ?k, ?v)  &*&  integer(key, _)  &*&  integer(value, _);
//@ ensures  pointer(rootptr, ?root1)  &*&  IsTree(root1, ?t1)  &*&  IsLLRB(t1, Blk, (result ? h0-1 : h0))  &*&  flatten(t1) == rest    &*&  integer(key, k)  &*&  integer(value, v);
{
  TreeNodePtr root = *rootptr;
  
  //@ open IsLLRB(t0, Blk, h0);
  //@ open IsTree(root, t0);
  //@ open Cons(flatten(t0), kv, rest);
  //@ open Pair(kv, k, v);
  
  //@ assert root->lft |-> ?lft  &*&  IsTree(lft, ?l0)  &*&  root->rgt |-> ?rgt  &*&  IsTree(rgt, ?r0);
  
  if (root->lft == 0) {
    *key = root->key;
    *value = root->value;
    
    //@ null_ptr_is_black_Leaf(root->lft);
    
    //@ black_0_has_no_elements(l0);
    //@ black_0_has_no_elements(r0);
    
    //@ dispose_null_ptr(root->lft);
    //@ dispose_black_0(root->rgt);
    freeTreeNode(root);
    *rootptr = 0;
    //@ close IsTree(0, Leaf);
    //@ close IsLLRB(Leaf, Blk, 0);
    
    return true;
  }
  else {
    //@ has_black_root_LLRB(l0);
    if (has_black_root(root->lft)) {
      //@ black_non_null_has_height(root->lft);
      //@ black_with_height_has_elements(l0);
    
      //@ close Cons(flatten(t0), kv, rest);
      //@ close Pair(kv, k, v);
    
      //@ leftmost_of_left_is_leftmost_of_whole(flatten(t0), flatten(l0), root->key, root->value, flatten(r0));
      //@ removeMin_left_is_removeMin_root(flatten(t0), flatten(l0), root->key, root->value, flatten(r0));
      bool fix = removeMinB(&(root->lft), key, value);
      
      //@ assert IsTree(root, ?t1);
      if (fix) {
        //@ close Branch(t1, _, _, _, Blk, _);
        return fix_short_left_B(rootptr);
      }
      else {
        //@ close IsLLRB(t1, Blk, h0);
        return false;
      }
    }
    else {
      //@ red_has_elements(l0);
      //@ close Cons(flatten(t0), kv, rest);
      //@ close Pair(kv, k, v);
    
      //@ leftmost_of_left_is_leftmost_of_whole(flatten(t0), flatten(l0), root->key, root->value, flatten(r0));
      //@ removeMin_left_is_removeMin_root(flatten(t0), flatten(l0), root->key, root->value, flatten(r0));
      bool fix = removeMinR(&(root->lft), key, value);
      //@ close IsTree(root, ?t1);
      //@ close Branch(t1, _, _, _, _, _);
      //@ b3_or_b2(t1, fix);
      return false;
    }
  }
}
// </removeMinB>

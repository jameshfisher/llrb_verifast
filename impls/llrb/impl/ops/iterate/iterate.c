#include "./iterate.h"

/*@
// <foreach_Tree>
predicate foreach_Tree<K,V>(Tree<K,V> T, predicate(Pair<K,V>) p) =
  switch (T) {
    case Leaf: return true;
    case Branch(L,k,v,c,R): return foreach_Tree(L, p) &*& p(Pair(k,v)) &*& foreach_Tree(R, p);
  };
// </foreach_Tree>

// <foreach_tree_2_index>
lemma void foreach_tree_2_index<K,V>(Tree<K,V> T0, predicate(Pair<K,V>) p)
  requires foreach_Tree(T0, p);
  ensures  foreach(flatten(T0), p);
{
  open foreach_Tree(T0, p);
  switch (T0) {
    case Leaf:
      close foreach(flatten(T0), p);
    case Branch(L0,k,v,c,R0):
      foreach_tree_2_index(L0, p);
      foreach_tree_2_index(R0, p);
      close foreach(cons(Pair(k,v), flatten(R0)), p);
      foreach_append(flatten(L0), cons(Pair(k,v), flatten(R0)));
  }
}
// </foreach_tree_2_index>
@*/

// <llrb_iterate_tree>
void llrb_iterate_tree(iterator * it, TreeNodePtr root)
//@ requires IsTree(root, ?T0)  &*&  is_iterator(it) == true;
//@ ensures  IsTree(root,  T0)  &*&  foreach_Tree(T0, iterator_post_ctor(it));
{
  //@ open IsTree(root, T0);
  
  if (root != 0) {
    llrb_iterate_tree(it, root->lft);
    it(root->key, root->value);
    //@ close iterator_post_ctor(it)(Pair(root->key, root->value));
    llrb_iterate_tree(it, root->rgt);
  }
  
  //@ close IsTree(root, T0);
  //@ close foreach_Tree(T0, iterator_post_ctor(it));
}
// </llrb_iterate_tree>

// <llrb_iterate>
void llrb_iterate(iterator * it, TreeNodePtr root)
//@ requires IsTree(root, ?T0)  &*&  is_iterator(it) == true;
//@ ensures  IsTree(root,  T0)  &*&  foreach(flatten(T0), iterator_post_ctor(it));
{
  llrb_iterate_tree(it, root);
  //@ foreach_tree_2_index(T0, iterator_post_ctor(it));
}
// </llrb_iterate>
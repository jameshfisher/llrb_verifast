#ifndef ITERATE_H
#define ITERATE_H

#include "../../../../../spec/iterate/iterate.h"
#include "../../data/node.h"
#include "../../spec/tree.h"
#include "../../spec/flatten.h"

// <llrb_iterate_declaration>
void llrb_iterate(iterator * it, TreeNodePtr root);
//@ requires IsTree(root, ?T0)  &*&  is_iterator(it) == true;
//@ ensures  IsTree(root,  T0)  &*&  foreach(flatten(T0), iterator_post_ctor(it));
// </llrb_iterate_declaration>

#endif
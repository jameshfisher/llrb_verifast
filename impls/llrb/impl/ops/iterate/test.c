#include "./iterate.h"

#include <stdio.h>

void printKeyValue(int k, int v) //@ : iterator
//@ requires true;
//@ ensures  iterator_post(printKeyValue)(Pair(k, v));
{
  printf("%i", k);
  printf("%i", v);
  //@ close iterator_post(printKeyValue)(Pair(k, v));
}

/*@
predicate_family_instance iterator_post(printKeyValue)(Pair<int,int> pair) = true;
@*/

void printTree(TreeNodePtr root)
//@ requires IsTree(root, ?T);
//@ ensures  IsTree(root,  T);
{
  llrb_iterate(printKeyValue, root);
  //@ leak foreach(flatten(T), iterator_post_ctor(printKeyValue));
}
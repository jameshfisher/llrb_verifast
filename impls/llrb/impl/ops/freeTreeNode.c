#include "freeTreeNode.h"

// <freeTreeNode>
void freeTreeNode(TreeNodePtr node)
//@ requires node->key |-> _  &*&  node->value |-> _  &*&  node->color |-> _  &*&  node->lft |-> _  &*&  node->rgt |-> _  &*&  malloc_block_TreeNode(node);
//@ ensures emp;
{
  free(node);
}
// </freeTreeNode>
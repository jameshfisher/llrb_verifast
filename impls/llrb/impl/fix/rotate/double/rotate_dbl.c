#include "rotate_dbl.h"

#include "./fixpoints.h"
#include "./lemmas.h"
#include "../single/lemmas.h"
#include "../single/rotate.h"

// <rotate_dbl_left_1>
TreeNodePtr rotate_dbl_left(TreeNodePtr root)
//@ requires  IsTree(root,   ?t0)  &*&  has_right_left_branch(t0) == true;
//@ ensures   IsTree(result, ?t1)  &*&  t1 == rotate_dbl_left_fixpoint(t0);
{
  //@ open IsTree(root, t0);
  //@ assert root->rgt |-> ?rgtptr &*& IsTree(rgtptr, ?r0);
  //@ rotate_right_is_branch(r0);
  TreeNodePtr newRgt = rotate_right(root->rgt);
  root->rgt = newRgt;
  //@ close IsTree(root, ?t1);
  return rotate_left(root);
}
// </rotate_dbl_left_1>

// <rotate_dbl_right_1>
TreeNodePtr rotate_dbl_right(TreeNodePtr root)
//@ requires  IsTree(root,   ?t0)  &*&  has_left_right_branch(t0) == true;
//@ ensures   IsTree(result, ?t1)  &*&  t1 == rotate_dbl_right_fixpoint(t0);
{
  //@ open IsTree(root, t0);
  //@ assert root->lft |-> ?lftptr &*& IsTree(lftptr, ?l0);
  //@ rotate_left_is_branch(l0);
  TreeNodePtr newLft = rotate_left(root->lft);
  root->lft = newLft;
  //@ close IsTree(root, ?t1);
  return rotate_right(root);
}
// </rotate_dbl_right_2>
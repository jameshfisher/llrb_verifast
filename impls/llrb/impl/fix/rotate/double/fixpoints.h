#ifndef ROTATE_DOUBLE_FIXPOINTS_H
#define ROTATE_DOUBLE_FIXPOINTS_H

#include "../../../data/node.h"
#include "../../../spec/fixpoints.h"
#include "../single/fixpoints.h"

/*@
// <rotate_dbl_left_fixpoint>
fixpoint Tree<K,V> rotate_dbl_left_fixpoint<K,V>(Tree<K,V> t)
// requires has_right_left_branch(t);
{
  switch (t) {
    case Leaf: return Leaf; // not possible
    case Branch(l,k,v,c,r):
      return rotate_left_fixpoint(Branch(l,k,v,c,rotate_right_fixpoint(r)));
  }
}
// </rotate_dbl_left_fixpoint>

// <rotate_dbl_right_fixpoint>
fixpoint Tree<K,V> rotate_dbl_right_fixpoint<K,V>(Tree<K,V> t)
// requires has_left_right_branch(t);
{
  switch (t) {
    case Leaf: return Leaf; // not possible
    case Branch(l,k,v,c,r):
      return rotate_right_fixpoint(Branch(rotate_left_fixpoint(l),k,v,c,r));
  }
}
// </rotate_dbl_right_fixpoint>
@*/

#endif
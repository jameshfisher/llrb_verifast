#ifndef ROTATE__DOUBLE__LEMMAS_H
#define ROTATE__DOUBLE__LEMMAS_H

#include "./fixpoints.h"
#include "../../../spec/flatten.h"

/*@
// <rotate_dbl_left_maintains_values>
lemma void rotate_dbl_left_maintains_values<K,V>(Tree<K,V> t);
  requires has_right_left_branch(t) == true;
  ensures  flatten(rotate_dbl_left_fixpoint(t)) == flatten(t);
// </rotate_dbl_left_maintains_values>

// <rotate_dbl_right_maintains_values>
lemma void rotate_dbl_right_maintains_values<K,V>(Tree<K,V> t);
  requires has_left_right_branch(t) == true;
  ensures  flatten(rotate_dbl_right_fixpoint(t)) == flatten(t);
// </rotate_dbl_right_maintains_values>

// <rotate_dbl_left_branches>
lemma void rotate_dbl_left_branches<K,V>(Tree<K,V> T);
  requires Branch(T, ?L, ?k, ?v, ?c, ?R) &*& Branch(R, ?RL, ?rk, ?rv, ?rc, ?RR) &*& Branch(RL, ?RLL, ?rlk, ?rlv, ?rlc, ?RLR);
  ensures  Branch(rotate_dbl_left_fixpoint(T), ?L1, rlk, rlv, rlc, ?R1) &*& Branch(L1, L, k, v, c, RLL) &*& Branch(R1, RLR, rk, rv, rc, RR);
// </rotate_dbl_left_branches>

// <rotate_dbl_right_branches>
lemma void rotate_dbl_right_branches<K,V>(Tree<K,V> T);
  requires Branch(T, ?L, ?k, ?v, ?c, ?R) &*& Branch(L, ?LL, ?lk, ?lv, ?lc, ?LR) &*& Branch(LR, ?LRL, ?lrk, ?lrv, ?lrc, ?LRR);
  ensures  Branch(rotate_dbl_right_fixpoint(T), ?L1, lrk, lrv, lrc, ?R1) &*& Branch(L1, LL, lk, lv, lc, LRL) &*& Branch(R1, LRR, k, v, c, R);
// </rotate_dbl_right_branches>
@*/

#endif
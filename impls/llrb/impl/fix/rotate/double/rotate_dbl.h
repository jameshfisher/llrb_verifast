#ifndef ROTATE_DBL_H
#define ROTATE_DBL_H

#include "./fixpoints.h"

// <rotate_dbl_left_declaration>
TreeNodePtr rotate_dbl_left(TreeNodePtr root);
//@ requires  IsTree(root,   ?t0)  &*&  has_right_left_branch(t0) == true;
//@ ensures   IsTree(result, ?t1)  &*&  t1 == rotate_dbl_left_fixpoint(t0);
// </rotate_dbl_left_declaration>

// <rotate_dbl_right_declaration>
TreeNodePtr rotate_dbl_right(TreeNodePtr root);
//@ requires  IsTree(root,   ?t0)  &*&  has_left_right_branch(t0) == true;
//@ ensures   IsTree(result, ?t1)  &*&  t1 == rotate_dbl_right_fixpoint(t0);
// </rotate_dbl_right_declaration>

#endif
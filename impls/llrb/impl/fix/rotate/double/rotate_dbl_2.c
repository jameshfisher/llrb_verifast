#include "rotate_dbl.h"

#include "./fixpoints.h"
#include "./lemmas.h"

// <rotate_dbl_left_2>
TreeNodePtr rotate_dbl_left(TreeNodePtr root)
//@ requires  IsTree(root,   ?t0)  &*&  has_right_left_branch(t0) == true;
//@ ensures   IsTree(result, ?t1)  &*&  t1 == rotate_dbl_left_fixpoint(t0);
{
  //@ open IsTree(root, t0);
  TreeNodePtr rgt = root->rgt;
  //@ open IsTree(rgt, _);
  TreeNodePtr rgt_lft = rgt->lft;
  //@ open IsTree(rgt_lft, _);
  TreeNodePtr rgt_lft_lft = rgt_lft->lft;
  TreeNodePtr rgt_lft_rgt = rgt_lft->rgt;
  
  rgt_lft->lft = root;
  rgt_lft->rgt = rgt;
  rgt->lft = rgt_lft_rgt;
  root->rgt = rgt_lft_lft;
  
  return rgt_lft;
}
// </rotate_dbl_left_2>

// <rotate_dbl_right_2>
TreeNodePtr rotate_dbl_right(TreeNodePtr root)
//@ requires  IsTree(root,   ?t0)  &*&  has_left_right_branch(t0) == true;
//@ ensures   IsTree(result, ?t1)  &*&  t1 == rotate_dbl_right_fixpoint(t0);
{
  //@ open IsTree(root, t0);
  TreeNodePtr lft = root->lft;
  //@ open IsTree(lft, _);
  TreeNodePtr lft_rgt = lft->rgt;
  //@ open IsTree(lft_rgt, _);
  TreeNodePtr lft_rgt_rgt = lft_rgt->rgt;
  TreeNodePtr lft_rgt_lft = lft_rgt->lft;
  
  lft_rgt->rgt = root;
  lft_rgt->lft = lft;
  lft->rgt = lft_rgt_lft;
  root->lft = lft_rgt_rgt;
  
  return lft_rgt;
}
// </rotate_dbl_right_2>
#ifndef ROTATE_FIXPOINT_H
#define ROTATE_FIXPOINT_H

#include "../../../spec/fixpoints.h"

/*@
// <rotate_left_fixpoint>
fixpoint Tree<K,V> rotate_left_fixpoint<K,V>(Tree<K,V> T)
// requires has_right_branch(t);
{
  switch (T) {
    case Leaf: return Leaf; // not possible
    case Branch(L, k, v, c, R):
      return switch (R) {
        case Leaf: return Leaf; // not possible
        case Branch(RL, rk, rv, rc, RR):
          return Branch(Branch(L, k, v, c, RL), rk, rv, rc, RR);
      };
  }
}
// </rotate_left_fixpoint>

// <rotate_right_fixpoint>
fixpoint Tree<K,V> rotate_right_fixpoint<K,V>(Tree<K,V> t)
// requires has_left_branch(t);
{
  switch (t) {
    case Leaf: return Leaf; // not possible
    case Branch(l,k,v,c,r):
      return switch (l) {
        case Leaf: return Leaf; // not possible
        case Branch(ll, lk, lv, lc, lr):
          return Branch(ll,lk,lv,lc,Branch(lr,k,v,c,r));
      };
  }
}
// </rotate_right_fixpoint>

@*/

#endif
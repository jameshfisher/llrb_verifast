#include "rotate.h"

// <rotate_left>
TreeNodePtr rotate_left(TreeNodePtr root)
//@ requires  IsTree(root,   ?t0)  &*&  has_right_branch(t0) == true;
//@ ensures   IsTree(result, ?t1)  &*&  t1 == rotate_left_fixpoint(t0);
{
  //@ open IsTree(root, _);
  TreeNodePtr rgt = root->rgt;
  //@ open IsTree(rgt, _);
  root->rgt = rgt->lft;
  //@ close IsTree(root, _);
  rgt->lft = root;
  //@ close IsTree(rgt,  _);
  
  return rgt;
}
// </rotate_left>

TreeNodePtr rotate_right(TreeNodePtr root)
//@ requires IsTree(root,   ?t0)   &*&  has_left_branch(t0) == true;
//@ ensures  IsTree(result, ?t1)   &*&  t1 == rotate_right_fixpoint(t0);
{
  //@ open IsTree(root, _);
  TreeNodePtr lft = root->lft;
  //@ open IsTree(lft, _);
  root->lft = lft->rgt;
  //@ close IsTree(root, _);
  lft->rgt = root;
  //@ close IsTree(lft, _);
  
  return lft;
}

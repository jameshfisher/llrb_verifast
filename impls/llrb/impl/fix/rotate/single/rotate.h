#ifndef ROTATE_H
#define ROTATE_H

#include "../../../data/node.h"
#include "./fixpoints.h"

// <rotate_left_declaration>
TreeNodePtr rotate_left(TreeNodePtr root);
//@ requires  IsTree(root,   ?t0)  &*&  has_right_branch(t0) == true;
//@ ensures   IsTree(result, ?t1)  &*&  t1 == rotate_left_fixpoint(t0);
// </rotate_left_declaration>

// <rotate_right_declaration>
TreeNodePtr rotate_right(TreeNodePtr root);
//@ requires IsTree(root,   ?t0)   &*&  has_left_branch(t0) == true;
//@ ensures  IsTree(result, ?t1)   &*&  t1 == rotate_right_fixpoint(t0);
// </rotate_right_declaration>

#endif
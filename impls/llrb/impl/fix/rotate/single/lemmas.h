#ifndef ROTATE_LEMMAS_H
#define ROTATE_LEMMAS_H

#include "./fixpoints.h"
#include "../../../spec/flatten.h"

/*@
// <rotate_left_maintains_values>
lemma void rotate_left_maintains_values<K,V>(Tree<K,V> t)
  requires has_right_branch(t) == true;
  ensures  flatten(rotate_left_fixpoint(t)) == flatten(t);
{
  switch (t) {
    case Leaf: // not possible
    case Branch(l,k,v,c,r):
      switch (r) {
        case Leaf: // not possible
        case Branch(rl, rk, rv, rc, rr):
          append_assoc(
            flatten(l),
            cons(Pair(k,v),flatten(rl)),
            cons(Pair(rk,rv), flatten(rr))
          );
      };
  }
}
// </rotate_left_maintains_values>

lemma void rotate_right_maintains_values<K,V>(Tree<K,V> t)
  requires has_left_branch(t) == true;
  ensures  flatten(rotate_right_fixpoint(t)) == flatten(t);
{
  switch (t) {
    case Leaf: // not possible
    case Branch(l,k,v,c,r):
      switch (l) {
        case Leaf: // not possible
        case Branch(ll, lk, lv, lc, lr):
          append_assoc(
            flatten(ll),
            cons(Pair(lk,lv), flatten(lr)),
            cons(Pair(k,v), flatten(r))
          );
      };
  }
}

lemma void rotate_right_is_branch<K,V>(Tree<K,V> t)
  requires has_left_branch(t) == true;
  ensures  is_branch(rotate_right_fixpoint(t)) == true;
{
  switch (t) {
    case Leaf: // not possible
    case Branch(l,k,v,c,r):
      switch (l) {
        case Leaf: // not possible
        case Branch(ll, lk, lv, lc, lr):
      }
  }
}

// <rotate_left_is_branch>
lemma void rotate_left_is_branch<K,V>(Tree<K,V> t);
  requires has_right_branch(t) == true;
  ensures  is_branch(rotate_left_fixpoint(t)) == true;
// </rotate_left_is_branch>

lemma void rotate_right_branches<K,V>(Tree<K,V> t0);
  requires Branch(t0, ?L, ?k, ?v, ?c, ?R0) &*& Branch(L, ?LL, ?lk, ?lv, ?lc, ?LR);
  ensures  Branch(rotate_right_fixpoint(t0), LL, lk, lv, lc, ?R1) &*& Branch(R1, LR, k, v, c, R0);


// <rotate_left_branches>
lemma void rotate_left_branches<K,V>(Tree<K,V> t0);
  requires Branch(t0, ?L, ?k, ?v, ?c, ?R0) &*& Branch(R0, ?RL, ?rk, ?rv, ?rc, ?RR);
  ensures  Branch(rotate_left_fixpoint(t0), ?L1, rk, rv, rc, RR) &*& Branch(L1, L, k, v, c, RL);
// </rotate_left_branches>

@*/

#endif
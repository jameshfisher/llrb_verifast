#ifndef LLRB_PREDICATES_H
#define LLRB_PREDICATES_H

#include "../spec/tree.h"
#include "../spec/balance/predicate.h"

/*@
// <B2_predicate>
predicate B2<K,V>(Tree<K,V> tree, int h)
  =    Branch(tree, ?l, _, _, Blk, ?r)
  &*&  IsLLRB(l, Blk, ?subh)
  &*&  IsLLRB(r, Blk,  subh)
  &*&  h == subh+1
  ;
// </B2_predicate>

// <B3_predicate>
predicate B3<K,V>(Tree<K,V> tree, int height)
  =    Branch(tree, ?l, _, _, Blk, ?r)
  &*&  IsLLRB(l, Red, ?subh)
  &*&  IsLLRB(r, Blk,  subh)
  &*&  height == subh+1
  ;
// </B3_predicate>

// <B3Right_predicate>
predicate B3Right<K,V>(Tree<K,V> tree, int height)
  =    Branch(tree, ?l, _, _, Blk, ?r)
  &*&  IsLLRB(l, Blk, ?subh)
  &*&  IsLLRB(r, Red,  subh)
  &*&  height == subh+1
  ;
// </B3Right_predicate>

// <B4_predicate>
predicate B4<K,V>(Tree<K,V> tree, int h)
  =    Branch(tree, ?l, _, _, Blk, ?r)
  &*&  IsLLRB(l, Red, ?subh)
  &*&  IsLLRB(r, Red,  subh)
  &*&  h == subh+1
  ;
// </B4_predicate>

// <RV_predicate>
predicate RV<K,V>(Tree<K,V> tree, int height)
  =    Branch(tree, ?l, _, _, Red, ?r)
  &*&  IsLLRB(l, Red, height)
  &*&  IsLLRB(r, Blk, height)
  ;
// </RV_predicate>

// <RVRight_predicate>
predicate RVRight<K,V>(Tree<K,V> tree, int h)
  =    Branch(tree, ?l, _, _, Red, ?r)
  &*&  IsLLRB(l, Blk, h)
  &*&  IsLLRB(r, Red, h)
  ;
// </RVRight_predicate>

// <RVLeft_predicate>
predicate RVLeft<K,V>(Tree<K,V> tree, int h)
  =    Branch(tree, ?l, _, _, Blk, ?r)
  &*&  RV(l, ?subh)
  &*&  IsLLRB(r, Blk, subh)
  &*&  h == subh+1
  ;
// </RVLeft_predicate>
@*/

#endif
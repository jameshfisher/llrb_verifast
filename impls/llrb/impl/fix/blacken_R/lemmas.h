#ifndef BLACKEN_R_LEMMAS_H
#define BLACKEN_R_LEMMAS_H

#include "./fixpoint.h"
#include "../predicates.h"
#include "../../spec/balance/predicate.h"
#include "../../spec/balance/lemmas.h"
#include "../../spec/flatten.h"

/*@
// <blacken_R_applied_to_R>
lemma void blacken_R_applied_to_R<K,V>(Tree<K,V> T)
  requires IsLLRB(T, Red, ?h);
  ensures  IsLLRB(blacken_R_fixpoint(T), Blk, h+1) &*& flatten(blacken_R_fixpoint(T)) == flatten(T);
{
  //IsLLRB_dup(T);
  open IsLLRB(T, Red, h);
  switch (T) {
    case Leaf: // not possible
    case Branch(L,k,v,c,R):  // c == Red
      close IsLLRB(blacken_R_fixpoint(T), Blk, h+1);
  };
}
// </blacken_R_applied_to_R>

// <blacken_R_applied_to_RV>
lemma void blacken_R_applied_to_RV<K,V>(Tree<K,V> T)
  requires RV(T, ?h);
  ensures  IsLLRB(blacken_R_fixpoint(T), Blk, h+1) &*& flatten(blacken_R_fixpoint(T)) == flatten(T);
{
  open RV(T, h);
  open Branch(T, _, _, _, _, _);
  switch (T) {
    case Leaf: // not possible
    case Branch(L,k,v,c,R):  // c == Red
      close IsLLRB(blacken_R_fixpoint(T), Blk, h+1);
  };
}
// </blacken_R_applied_to_RV>
@*/

#endif
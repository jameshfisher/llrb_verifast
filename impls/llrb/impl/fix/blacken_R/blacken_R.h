#ifndef BLACKEN_R_H
#define BLACKEN_R_H

#include "./fixpoint.h"
#include "../../data/node.h"
#include "../../spec/balance/predicate.h"
#include "../../spec/fixpoints.h"

// <blacken_R_declaration>
void blacken_R(TreeNodePtr root);
//@ requires IsTree(root, ?t0)  &*&  is_branch(t0) == true;
//@ ensures  IsTree(root, ?t1)  &*&  t1 == blacken_R_fixpoint(t0);
// </blacken_R_declaration>

#endif

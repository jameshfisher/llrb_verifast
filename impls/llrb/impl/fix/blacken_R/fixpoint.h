#ifndef BLACKEN_R_FIXPOINT_H
#define BLACKEN_R_FIXPOINT_H

#include "../../spec/bst.h"

/*@
// <blacken_R_fixpoint>
fixpoint Tree<K,V> blacken_R_fixpoint<K,V>(Tree<K,V> t)
// requires IsLLRB(t, Red, ?h);
{
  return switch (t) {
    case Leaf: return Leaf; // not possible
    case Branch(l,k,v,c,r): // c == Red
      return Branch(l,k,v,Blk,r);
  };
}
// </blacken_R_fixpoint>
@*/

#endif
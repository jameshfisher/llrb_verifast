#include "blacken_R.h"

// <blacken_R>
void blacken_R(TreeNodePtr root)
//@ requires IsTree(root, ?t0)  &*&  is_branch(t0) == true;
//@ ensures  IsTree(root, ?t1)  &*&  t1 == blacken_R_fixpoint(t0);
{
  //@ open IsTree(root, t0);
  root->color = Blk;
  //@ close IsTree(root, _);
}
// </blacken_R>
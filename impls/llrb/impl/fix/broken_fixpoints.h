
fixpoint bool fix_B3Right_precond<K,V>(Tree<K,V> t) {
  return switch (t) {
    case Leaf: return false;
    case Branch(l,k,v,c,r): // c == Blk
      return c == Blk && switch (r) {
        case Leaf: return false;
        case Branch(rl,rk,rv,rc,rr): return rc == Red;
      };
  };
}


fixpoint bool is_RVLeft(Tree<K,V> t) {
  return switch (t) {
    case Leaf: return false;
    case Branch(l,k,v,c,r): return c == Blk && is_RV(l);
  }
}


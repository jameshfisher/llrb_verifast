#include "has_black_root.h"

// <has_black_root>
bool has_black_root(TreeNodePtr root)
//@ requires IsTree(root, ?t);
//@ ensures  IsTree(root,  t)  &*&  result == has_black_root_fixpoint(t);
{
  //@ open IsTree(root, t);
  if (root == 0) {
    //@ close IsTree(root, t);
    return true;
  }
  else {
    bool result = root->color == Blk;
    //@ close IsTree(root, t);
    return result;
  }
}
// </has_black_root>
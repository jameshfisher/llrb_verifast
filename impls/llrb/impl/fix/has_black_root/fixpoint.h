#ifndef FIX_HAS_BLACK_ROOT_H
#define FIX_HAS_BLACK_ROOT_H

#include "../../spec/tree.h"

/*@
// <has_black_root_fixpoint>
fixpoint bool has_black_root_fixpoint<K,V>(Tree<K,V> t) {
  switch (t) {
    case Leaf: return true;
    case Branch(l,k,v,c,r): return c == Blk;
  }
}
// </has_black_root_fixpoint>
@*/

#endif
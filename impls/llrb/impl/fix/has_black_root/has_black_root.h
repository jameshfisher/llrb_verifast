#ifndef HAS_BLACK_ROOT_H
#define HAS_BLACK_ROOT_H

#include "../../data/node.h"
#include "../../spec/balance/predicate.h"
#include "./fixpoint.h"

// <has_black_root_declaration>
bool has_black_root(TreeNodePtr root);
//@ requires IsTree(root, ?T);
//@ ensures  IsTree(root,  T)  &*&  result == has_black_root_fixpoint(T);
// </has_black_root_declaration>

#endif  // HAS_BLACK_ROOT_H
#ifndef LLRB__IMPL__FIX__HAS_BLACK_ROOT_LEMMAS_H
#define LLRB__IMPL__FIX__HAS_BLACK_ROOT_LEMMAS_H

#include "../../spec/tree.h"
#include "../../spec/balance/predicate.h"
#include "fixpoint.h"

/*@
// <has_black_root_LLRB>
lemma void has_black_root_LLRB<K,V>(Tree<K,V> t)
  requires IsLLRB(t, ?c, ?h);
  ensures  IsLLRB(t, has_black_root_fixpoint(t) ? Blk : Red, h);
{
  open IsLLRB(t, c, h);
  close IsLLRB(t, c, h);
}
// </has_black_root_LLRB>
@*/

#endif
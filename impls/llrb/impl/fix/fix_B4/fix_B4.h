#ifndef FIX_B4_H
#define FIX_B4_H

#include "../../data/node.h"
#include "../../spec/flatten.h"
#include "../predicates.h"

// <fix_B4_declaration>
void fix_B4(TreeNodePtr root);
//@ requires IsTree(root, ?t0)  &*&  B4(t0, ?h);
//@ ensures  IsTree(root, ?t1)  &*&  IsLLRB(t1, Red, h)  &*&  flatten(t1) == flatten(t0);
// </fix_B4_declaration>

#endif
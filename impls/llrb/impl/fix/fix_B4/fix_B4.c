#include "./fix_B4.h"

#include "../blacken_R/blacken_R.h"
#include "../blacken_R/lemmas.h"
#include "../../spec/balance/lemmas.h"

// <fix_B4>
void fix_B4(TreeNodePtr root)
//@ requires IsTree(root, ?t0)  &*&  B4(t0, ?h);
//@ ensures  IsTree(root, ?t1)  &*&  IsLLRB(t1, Red, h)  &*&  flatten(t1) == flatten(t0);
{
  //@ open B4(t0, h);
  //@ open Branch(t0, ?L, _, _, _, ?R);
  //@ open IsTree(root, t0);
  //@ red_has_branch(L);
  //@ red_has_branch(R);
  //@ blacken_R_applied_to_R(L);
  //@ blacken_R_applied_to_R(R);
  blacken_R(root->lft);
  blacken_R(root->rgt);
  root->color = Red;
  //@ close IsTree(root, ?t1);
  //@ close IsLLRB(t1, Red, h);
}
// </fix_B4>
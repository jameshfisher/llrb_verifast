#ifndef FIX_RV_RIGHT__LEMMA_H
#define FIX_RV_RIGHT__LEMMA_H

#include "../predicates.h"
#include "../rotate/single/fixpoints.h"
#include "../../spec/fixpoints.h"
#include "../../spec/balance/lemmas.h"

/*@
// <rotate_left_fixes_RVRight>
lemma void rotate_left_fixes_RVRight<K,V>(Tree<K,V> t0)
  requires RVRight(t0, ?h);
  ensures  RV(rotate_left_fixpoint(t0), h);
{
  open RVRight(t0, h);
  open Branch(t0, ?l, ?k, ?v, Red, ?r);
  open IsLLRB(r, Red, h);
  close Branch(r, ?rl, _, _, _, _);
  close Branch(rotate_left_fixpoint(t0), _, _, _, Red, _);
  close IsLLRB(Branch(l, k, v, Red, rl), Red, h);
  close RV(rotate_left_fixpoint(t0), h);
  open Branch(r, rl, _, _, _, _);
}
// </rotate_left_fixes_RVRight>

// <RVRight_has_right_branch>
lemma void RVRight_has_right_branch<K,V>(Tree<K,V> t0)
  requires RVRight(t0, ?h);
  ensures  RVRight(t0,  h) &*& has_right_branch(t0) == true;
{
  open RVRight(t0, h);
  open Branch(t0, _, _, _, _, _);
  
  switch (t0) {
    case Leaf: // not possible
    case Branch(l, k, v, c, r):
      red_has_branch(r);
      assert is_branch(r) == true;
      close Branch(t0, _, _, _, _, _);
      close RVRight(t0, h);
  }
}
// </RVRight_has_right_branch>
@*/

#endif
﻿#include "./fix_short_right_B.h"

#include "../../spec/balance/lemmas.h"
#include "../has_black_root/has_black_root.h"
#include "../has_black_root/lemmas.h"
#include "../blacken_R/blacken_R.h"
#include "../blacken_R/lemmas.h"
#include "../rotate/single/rotate.h"
#include "../rotate/single/lemmas.h"
#include "../rotate/double/rotate_dbl.h"
#include "../rotate/double/lemmas.h"
//#include "../../spec/balance/lemmas.h"

// The horror … the horror …
// <fix_short_right_B>
bool fix_short_right_B(TreeNodePtr * rootptr)
//@ requires pointer(rootptr, ?root0)  &*&  IsTree(root0, ?t0)  &*&  Branch(t0, ?l0, ?k, ?v, Blk, ?r0)  &*&  IsLLRB(l0, ?lc, ?h0)  &*&  IsLLRB(r0, Blk, h0-1);
//@ ensures  pointer(rootptr, ?root1)  &*&  IsTree(root1, ?t1)  &*&  IsLLRB(t1, Blk, (result ? h0 : h0+1))  &*&  flatten(t1) == flatten(t0);
{
  TreeNodePtr root = *rootptr;
  
  //@ height_gte_0(r0);
  
  //@ open Branch(t0, l0, k, v, Blk, r0);
  //@ open IsTree(root, t0);
  TreeNodePtr lft = root->lft;
  
  //@ has_black_root_LLRB(l0);
  if (has_black_root(lft)) {
    //@ open IsLLRB(l0, Blk, h0);
    //@ open IsTree(lft, l0);
    TreeNodePtr lftlft = lft->lft;
    //@ assert IsTree(lftlft, ?ll1);
    //@ has_black_root_LLRB(ll1);
    if (has_black_root(lftlft)) {
      lft->color = Red;
      //@ close IsTree(lft, _);
      //@ assert IsTree(lft, ?l1);
      //@ close IsLLRB(l1, Red, h0-1);
      //@ close IsTree(root, _);
      //@ assert IsTree(root, ?t1);
      //@ close IsLLRB(t1, Blk, h0);
      return true;
    }
    else {
      //@ red_has_branch(ll1);
      //@ blacken_R_applied_to_R(ll1);
      blacken_R(lftlft);
      //@ close IsTree(lft, ?l1);
      //@ close IsTree(root, ?t1);
      //@ close Branch(l1, _, _, _, Blk, _);
      //@ close Branch(t1, l1, k, v, Blk, r0);
      //@ rotate_right_branches(t1);
      //@ rotate_right_maintains_values(t1);
      root = rotate_right(root);
      //@ assert IsTree(root, ?t2);
      //@ open Branch(t2, _, _, _, Blk, ?r2);
      //@ open Branch(r2, _, _, _, Blk, r0);
      //@ close IsLLRB(r2, Blk, h0);
      //@ close IsLLRB(t2, Blk, h0+1);
      *rootptr = root;
      return false;
    }
  }
  else {
    //@ open IsLLRB(l0, Red, h0);
    //@ open IsTree(lft, l0);
    TreeNodePtr lftrgt = lft->rgt;
    //@ open IsTree(lftrgt, ?lr0);
    //@ open IsLLRB(lr0, Blk, h0);
    TreeNodePtr lftrgtlft = lftrgt->lft;
    //@ assert IsTree(lftrgtlft, ?lrl1);
    //@ has_black_root_LLRB(lrl1);
    if (has_black_root(lftrgtlft)) {
      lftrgt->color = Red;
      //@ close IsTree(lftrgt, ?lr1);
      //@ close IsLLRB(lr1, Red, h0-1);
      lft->color = Blk;
      //@ close IsTree(lft, ?l1);
      //@ close Branch(l1, _, _, _, Blk, lr1);
      //@ close IsTree(root, ?t1);
      //@ close Branch(t1, l1, k, v, Blk, r0);
      //@ rotate_right_branches(t1);
      //@ rotate_right_maintains_values(t1);
      root = rotate_right(root);
      //@ assert IsTree(root, ?t2);
      //@ open Branch(t2, _, _, _, Blk, ?r2);
      //@ open Branch(r2, lr1, _, _, Blk, r0);
      //@ close IsLLRB(r2, Blk, h0);
      //@ close IsLLRB(t2, Blk, h0+1);
      *rootptr = root;
      return false;
    }
    else {
      //@ red_has_branch(lrl1);
      //@ blacken_R_applied_to_R(lrl1);
      blacken_R(lftrgtlft);
      //@ close IsTree(lftrgt, ?lr1);
      //@ close IsTree(lft, ?l1);
      //@ close IsTree(root, ?t1);
      //@ close Branch(lr1, _, _, _, Blk, _);
      //@ close Branch(l1, _, _, _, Red, lr1);
      //@ close Branch(t1, l1, _, _, Blk, r0);
      //@ rotate_dbl_right_branches(t1);
      //@ rotate_dbl_right_maintains_values(t1);
      root = rotate_dbl_right(root);
      //@ assert IsTree(root, ?t2);
      //@ open Branch(t2, ?l2, _, _, Blk, ?r2);
      //@ open Branch(l2, _, _, _, Red, _);
      //@ open Branch(r2, _, _, _, Blk, _);
      //@ close IsLLRB(l2, Red, h0);
      //@ close IsLLRB(r2, Blk, h0);
      //@ close IsLLRB(t2, Blk, h0+1);
      *rootptr = root;
      return false;
    }
  }
}
// </fix_short_right_B>
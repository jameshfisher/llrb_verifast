#ifndef FIX_SHORT_RIGHT_B_H
#define FIX_SHORT_RIGHT_B_H

#include "../../data/node.h"
#include "../../spec/balance/predicate.h"
#include "../../spec/flatten.h"

// <fix_short_right_B_declaration>
bool fix_short_right_B(TreeNodePtr * rootptr);
//@ requires pointer(rootptr, ?root0)  &*&  IsTree(root0, ?t0)  &*&  Branch(t0, ?l0, ?k, ?v, Blk, ?r0)  &*&  IsLLRB(l0, ?lc, ?h0)  &*&  IsLLRB(r0, Blk, h0-1);
//@ ensures  pointer(rootptr, ?root1)  &*&  IsTree(root1, ?t1)  &*&  IsLLRB(t1, Blk, (result ? h0 : h0+1))  &*&  flatten(t1) == flatten(t0);
// </fix_short_right_B_declaration>

#endif
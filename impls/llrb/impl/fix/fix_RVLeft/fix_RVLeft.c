#include "./fix_RVLeft.h"

#include "../rotate/single/rotate.h"
#include "../rotate/single/lemmas.h"
#include "../blacken_R/blacken_R.h"
#include "../blacken_R/lemmas.h"
#include "../../spec/balance/lemmas.h"

/*@
lemma void RVLeft_has_left_branch<K,V>(Tree<K,V> t0);
  requires RVLeft(t0, ?h);
  ensures  RVLeft(t0,  h) &*& has_left_branch(t0) == true;

@*/

/*
// <fix_RVLeft_unverified>
TreeNodePtr fix_RVLeft(TreeNodePtr root)
{
  TreeNodePtr result = rotate_right(root);
  TreeNodePtr lft = result->lft;
  blacken_R(lft);
  return result;
}
// </fix_RVLeft_unverified>
*/

// <fix_RVLeft>
TreeNodePtr fix_RVLeft(TreeNodePtr root)
//@ requires IsTree(root,   ?t0)  &*&  RVLeft(t0,     ?h);
//@ ensures  IsTree(result, ?t1)  &*&  IsLLRB(t1, Red, h)  &*&  flatten(t1) == flatten(t0);
{
  //@ RVLeft_has_left_branch(t0);
  //@ open RVLeft(t0, h);
  //@ open RV(_, h-1);
  TreeNodePtr result = rotate_right(root);
  //@ rotate_right_branches(t0);
  //@ rotate_right_maintains_values(t0);
  //@ open Branch(?t1, ?l, ?dk, ?dv, Red, ?r);
  //@ open Branch(r, _, _, _, Blk, _);
  //@ close IsLLRB(r, Blk, _);
  
  //@ open IsTree(result, t1);
  TreeNodePtr lft = result->lft;
  //@ red_has_branch(l);
  blacken_R(lft);
  //@ blacken_R_applied_to_R(l);
  //@ assert IsTree(lft, ?newl);
  //@ close IsLLRB(Branch(newl, dk, dv, Red, r), Red, h);
  //@ close IsTree(result, _);
  return result;
}
// </fix_RVLeft>

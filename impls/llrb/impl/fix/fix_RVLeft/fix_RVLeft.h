#ifndef FIX_H
#define FIX_H

#include "../../data/node.h"
#include "../predicates.h"
#include "../../spec/flatten.h"

// <fix_RVLeft_declaration>
TreeNodePtr fix_RVLeft(TreeNodePtr root);
//@ requires IsTree(root,   ?t0)  &*&  RVLeft(t0,     ?h);
//@ ensures  IsTree(result, ?t1)  &*&  IsLLRB(t1, Red, h)  &*&  flatten(t1) == flatten(t0);
// </fix_RVLeft_declaration>

#endif
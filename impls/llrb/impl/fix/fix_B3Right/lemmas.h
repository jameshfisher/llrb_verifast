#ifndef FIX_B3RIGHT_LEMMAS_H
#define FIX_B3RIGHT_LEMMAS_H

#include "./fixpoint.h"
#include "../predicates.h"
#include "../../spec/flatten.h"
#include "../rotate/single/lemmas.h"

/*@
// <fix_B3Right_lemma>
lemma void fix_B3Right_lemma<K,V>(Tree<K,V> t)
requires B3Right(t, ?h);
ensures  B3(fix_B3Right_fixpoint(t), h) &*& flatten(fix_B3Right_fixpoint(t)) == flatten(t);
{
  open B3Right(t, h);
  open Branch(t, ?l, _, _, Blk, ?r);
  switch (t) {
    case Leaf:
    case Branch(l',k,v,c,r'):
      open IsLLRB(r, Red, h-1);
      switch (r') {
        case Leaf:
        case Branch(rl,rk,rv,rc,rr):
          rotate_left_maintains_values(t);
          close Branch(fix_B3Right_fixpoint(t), ?l1, _, _, _, ?r1);
          close IsLLRB(l1, Red, h-1);
          close B3(fix_B3Right_fixpoint(t), h);
      };
  };
}
// </fix_B3Right_lemma>
@*/

#endif
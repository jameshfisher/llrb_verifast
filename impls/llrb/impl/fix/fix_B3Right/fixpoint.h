#ifndef FIX_B3RIGHT_FIXPOINT_H
#define FIX_B3RIGHT_FIXPOINT_H

#include "../../spec/tree.h"
#include "../rotate/single/rotate.h"

/*@
// <fix_B3Right_fixpoint>
fixpoint Tree<K,V> fix_B3Right_fixpoint<K,V>(Tree<K,V> T)
{
  return switch (T) {
    case Leaf: return Leaf; // not possible
    case Branch(L, k, v, c, R): // c == Blk
      return switch (R) {
        case Leaf: return Leaf; // not possible
        case Branch(RL, rk, rv, rc, RR): // rc == Red
          return rotate_left_fixpoint(Branch(L, k, v, Red, Branch(RL, rk, rv, Blk, RR)));
      };
  };
}
// </fix_B3Right_fixpoint>
@*/

#endif
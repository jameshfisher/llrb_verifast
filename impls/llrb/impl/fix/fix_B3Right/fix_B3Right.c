#include "fix_B3Right.h"
#include "../rotate/single/lemmas.h"
#include "../../spec/balance/lemmas.h"

// <fix_B3Right>
TreeNodePtr fix_B3Right(TreeNodePtr root)
//@ requires IsTree(root,   ?T0)  &*&  B3Right(T0, ?h);
//@ ensures  IsTree(result, fix_B3Right_fixpoint(T0));
{
  //@ open B3Right(T0, h);
  //@ open Branch(T0, ?L, ?k, ?v, Blk, ?R0);
  
  //@ open IsTree(root, T0);
  //@ red_has_branch(R0);
  //@ open IsTree(root->rgt, R0);
  root->color = Red;
  root->rgt->color = Blk;
  //@ close IsTree(root->rgt, ?R1);
  //@ close IsTree(root, ?T1);
  
  TreeNodePtr result = rotate_left(root);
  //@ rotate_left_is_branch(T1);
  //@ rotate_left_maintains_values(T1);
  
  //@ llrb_emp(L);
  //@ llrb_emp(R0);
  
  return result;
}
// </fix_B3Right>

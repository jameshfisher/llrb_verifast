#ifndef FIX_B3RIGHT_H
#define FIX_B3RIGHT_H

#include "./fixpoint.h"
#include "../predicates.h"

// <fix_B3Right_declaration>
TreeNodePtr fix_B3Right(TreeNodePtr root);
//@ requires IsTree(root,   ?t0)  &*&  B3Right(t0, ?h);
//@ ensures  IsTree(result, fix_B3Right_fixpoint(t0))  &*&  B3Right(t0, h);
// </fix_B3Right_declaration>

#endif
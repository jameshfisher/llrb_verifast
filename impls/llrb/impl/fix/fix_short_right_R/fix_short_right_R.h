#ifndef FIX_SHORT_RIGHT_R_H
#define FIX_SHORT_RIGHT_R_H

#include "../../data/node.h"
#include "../../spec/balance/predicate.h"
#include "../../spec/flatten.h"

// <fix_short_right_R_declaration>
bool fix_short_right_R(TreeNodePtr * rootptr);
//@ requires pointer(rootptr, ?root0)  &*&  IsTree(root0, ?t0)  &*&  Branch(t0, ?l0, ?k, ?v, Red, ?r0)  &*&  IsLLRB(r0, Blk, ?h0)  &*&  IsLLRB(l0, Blk, h0+1);
//@ ensures  pointer(rootptr, ?root1)  &*&  IsTree(root1, ?t1)  &*&  IsLLRB(t1, (result ? Blk : Red), h0+1)  &*&  flatten(t1) == flatten(t0);
// </fix_short_right_R_declaration>

#endif
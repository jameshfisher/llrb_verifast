#include "./fix_short_right_R.h"

#include "../../spec/balance/lemmas.h"
#include "../has_black_root/has_black_root.h"
#include "../has_black_root/lemmas.h"
#include "../fix_B4/fix_B4.h"
#include "../rotate/single/rotate.h"
#include "../rotate/single/lemmas.h"
#include "../predicates.h"

// <fix_short_right_R>
bool fix_short_right_R(TreeNodePtr * rootptr)
//@ requires pointer(rootptr, ?root0)  &*&  IsTree(root0, ?t0)  &*&  Branch(t0, ?l0, ?k, ?v, Red, ?r0)  &*&  IsLLRB(r0, Blk, ?h0)  &*&  IsLLRB(l0, Blk, h0+1);
//@ ensures  pointer(rootptr, ?root1)  &*&  IsTree(root1, ?t1)  &*&  IsLLRB(t1, (result ? Blk : Red), h0+1)  &*&  flatten(t1) == flatten(t0);
{
  TreeNodePtr root = *rootptr;
  //@ open Branch(t0, l0, k, v, Red, r0);
  //@ open IsTree(root, t0);
  
  //@ height_gte_0(r0);
  //@ open IsLLRB(l0, Blk, h0+1);
  
  TreeNodePtr lft = root->lft;
  //@ open IsTree(lft, l0);
  
  TreeNodePtr lftlft = lft->lft;
  //@ assert IsTree(lftlft, ?ll0);
  
  //@ has_black_root_LLRB(ll0);
  if (has_black_root(lftlft)) {
    root->color = Blk;
    lft->color = Red;  
    //@ close IsTree(lft, _);
    //@ assert IsTree(lft, ?l1);
    //@ close IsLLRB(l1, Red, h0);
    //@ close IsTree(root, _);
    //@ assert IsTree(root, ?t1);
    //@ close IsLLRB(t1, Blk, h0+1);
    return true;
  }
  else {
    //@ open IsTree(lftlft, ll0);
    //@ open IsLLRB(ll0, Red, h0);
    //@ close Branch(ll0, _, _, _, Red, _);
    //@ close Branch(l0, ll0, _, _, Blk, _);
    //@ close Branch(t0, l0, _, _, Red, _);
    //@ rotate_right_branches(t0);
    //@ rotate_right_maintains_values(t0);
    root = rotate_right(root);
    //@ assert IsTree(root, ?t1);
    //@ assert Branch(t1, ?l1, _, _, Blk, ?r1);
    //@ open Branch(l1, _, _, _, Red, _);
    //@ open Branch(r1, _, _, _, Red, _);
    //@ close IsLLRB(l1, Red, h0);
    //@ close IsLLRB(r1, Red, h0);
    //@ close B4(t1, h0+1);
    fix_B4(root);
    
    *rootptr = root;
    return false;
  }
}
// </fix_short_right_R>
#include "./fix_short_left_B.h"

#include "../../spec/balance/lemmas.h"
#include "../has_black_root/has_black_root.h"
#include "../has_black_root/lemmas.h"
#include "../rotate/single/rotate.h"
#include "../rotate/single/lemmas.h"
#include "../rotate/double/rotate_dbl.h"
#include "../rotate/double/lemmas.h"

// <fix_short_left_B>
bool fix_short_left_B(TreeNodePtr * rootptr)
//@ requires pointer(rootptr, ?root0)  &*&  IsTree(root0, ?t0)  &*&  Branch(t0, ?l0, ?k, ?v, Blk, ?r0)  &*&  IsLLRB(l0, Blk, ?h0)  &*&  IsLLRB(r0, Blk, h0+1);
//@ ensures  pointer(rootptr, ?root1)  &*&  IsTree(root1, ?t1)  &*&  IsLLRB(t1, Blk, (result ? h0+1 : h0+2))  &*&  flatten(t1) == flatten(t0);
{
  TreeNodePtr root = *rootptr;
  //@ open Branch(t0, l0, k, v, Blk, r0);
  //@ open IsTree(root0, t0);
  
  TreeNodePtr rgt = root->rgt;
  
  //@ height_gte_0(l0);
  //@ open IsLLRB(r0, Blk, h0+1);
  //@ open IsTree(rgt, r0);
  
  TreeNodePtr rgtlft = rgt->lft;
  //@ assert IsTree(rgtlft, ?rl0);
  
  //@ has_black_root_LLRB(rl0);
  if (has_black_root(rgtlft)) {
    root->color = Red;
    //@ assert IsTree(root, ?t0_5);
    //@ close  Branch(t0_5, l0, _, _, Red, r0);
    //@ close Branch(r0, _, _, _, Blk, _);
    //@ rotate_left_branches(t0_5);
    //@ rotate_left_maintains_values(t0_5);
    root = rotate_left(root);
    //@ assert IsTree(root, ?t1);
    //@ open Branch(t1, ?l1, _, _, Blk, _);
    //@ open Branch(l1, l0, _, _, Red, _);
        
    //@ close IsLLRB(l1, Red, h0);
    //@ close IsLLRB(t1, Blk, h0+1);
      
    *rootptr = root;
    return true;
  }
  else {
    //@ open IsTree(rgtlft, rl0);
    //@ open IsLLRB(rl0, Red, h0);
    //@ assert IsTree(root, ?t1);
    
    //@ close Branch(t1, l0, k, v, Blk, r0);
    //@ close Branch(r0, _, _, _, _, _);
    //@ assert Branch(r0, rl0, _, _, Blk, _);
    //@ close Branch(rl0, _, _, _, Red, _);
    //@ rotate_dbl_left_branches(t1);
    //@ rotate_dbl_left_maintains_values(t1);
    root = rotate_dbl_left(root);
    //@ assert IsTree(root, ?t2);
    //@ open Branch(t2, ?l2, _, _, Red, ?r2);
    //@ open Branch(l2, l0, _, _, Blk, _);
    //@ open Branch(r2, _, _, _, Blk, _);

    //@ open IsTree(root, t2);
    root->color = Blk;
    //@ close IsTree(root, _);
    //@ assert IsTree(root, ?t3);
    
    //@ close IsLLRB(l2, Blk, h0+1);
    //@ close IsLLRB(r2, Blk, h0+1);
    //@ close IsLLRB(t3, Blk, h0+2);
    *rootptr = root;
    return false;
  }
}
// </fix_short_left_B>
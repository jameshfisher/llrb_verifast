#ifndef TREE_FIXPOINTS_H
#define TREE_FIXPOINTS_H

#include "./tree.h"

/*@
// <tree_lowerBound>
fixpoint bool tree_lowerBound<V>(int lb, Tree<int,V> t)
{
  switch (t) {
    case Leaf: return true;
    case Branch(l, k, v, c, r): return
      tree_lowerBound(lb, l) &&
      tree_lowerBound(lb, r) &&
      lb < k;
  }
}
// </tree_lowerBound>

// <tree_higherBound>
fixpoint bool tree_higherBound<V>(Tree<int,V> t, int hb)
{
  switch (t) {
    case Leaf: return true;
    case Branch(l, k, v, c, r): return
      tree_higherBound(l, hb) &&
      tree_higherBound(r, hb) &&
      k < hb;
  }
}
// </tree_higherBound>

// <bst>
fixpoint bool bst<V>(Tree<int,V> t)
{
  switch (t) {
    case Leaf: return true;
    case Branch(l, k, v, c, r):
      return
        tree_higherBound(l, k)  &&
        tree_lowerBound(k, r)  &&
        bst(l)  &&  bst(r);
  }
}
// </bst>
@*/

#endif
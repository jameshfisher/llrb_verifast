#ifndef FIXPOINTS_H
#define FIXPOINTS_H

#include "./ctx.h"

/*@
// <ctx_compose>
fixpoint Tree<K,V> ctx_compose<K,V>(Ctx<K,V> C0, Tree<K,V> T0) {
  switch (C0) {
    case NilCtx: return T0;
    case LftCtx(l, k, v, c, pctx):
      return ctx_compose(pctx, Branch(l, k, v, c, T0));
    case RgtCtx(k, v, c, r, pctx):
      return ctx_compose(pctx, Branch(T0, k, v, c, r));
  }
}
// </ctx_compose>
@*/

#endif
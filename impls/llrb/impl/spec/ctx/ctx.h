#ifndef CTX_H
#define CTX_H

#include "../tree.h"

/*@
// <Ctx>
inductive Ctx<K,V> =
  | NilCtx
  | LftCtx(Tree<K,V>, K, V, Color, Ctx<K,V>)
  | RgtCtx(K, V, Color, Tree<K,V>, Ctx<K,V>);
// </Ctx>
@*/

#endif
#ifndef TREE_CTX_LEMMAS_H
#define TREE_CTX_LEMMAS_H

#include "./ctx.h"
#include "./fixpoints.h"
#include "../../spec/tree.h"
#include "../../spec/flatten.h"
#include "../../../../../spec/fixpoints.h"
#include "../../../../../spec/lemmas.h"

/*@
// <sorted_tree_sorted_subtree>
lemma void sorted_tree_sorted_subtree<V>(Tree<int,V> t, Ctx<int,V> ctx, Tree<int,V> subt)
  requires sorted(flatten(t)) == true  &*&  ctx_compose(ctx, subt) == t;
  ensures  sorted(flatten(subt)) == true;
{
  switch (ctx) {
    case NilCtx:
    case LftCtx(l, k, v, c, pctx):
      sorted_tree_sorted_subtree(t, pctx, Branch(l, k, v, c, subt));
      sides_of_sorted_are_sorted(flatten(l), cons(Pair(k,v), flatten(subt)));
    case RgtCtx(k, v, c, r, pctx):
      sorted_tree_sorted_subtree(t, pctx, Branch(subt, k, v, c, r));
      sides_of_sorted_are_sorted(flatten(subt), cons(Pair(k,v), flatten(r)));
  }
}
// </sorted_tree_sorted_subtree>
@*/

#endif
#ifndef BST_LEMMAS_H
#define BST_LEMMAS_H

#include "./bst.h"
#include "./flatten.h"
#include "../../../../spec/index.h"
#include "../../../../spec/lemmas.h"

/*@
// <tree_higherBound_is_list_higherBound>
lemma void tree_higherBound_is_list_higherBound<V>(Tree<int,V> t, int hb)
  requires tree_higherBound(t, hb) == true;
  ensures higherBound(flatten(t), hb) == true;
{
  switch (t) {
    case Leaf:
    case Branch(l, k, v, c, r):
      tree_higherBound_is_list_higherBound(l, hb);
      tree_higherBound_is_list_higherBound(r, hb);
      higherBound_sides_is_higherBound_whole(flatten(l), k, v, flatten(r), hb);
  }
}
// </tree_higherBound_is_list_higherBound>

// <tree_lowerBound_is_list_lowerBound>
lemma void tree_lowerBound_is_list_lowerBound<V>(int lb, Tree<int,V> t)
  requires tree_lowerBound(lb, t) == true;
  ensures lowerBound(lb, flatten(t)) == true;
{
  switch (t) {
    case Leaf:
    case Branch(l, k, v, c, r):
      tree_lowerBound_is_list_lowerBound(lb, l);
      tree_lowerBound_is_list_lowerBound(lb, r);
      lowerBound_sides_is_lowerBound_whole(lb, flatten(l), k, v, flatten(r));
  }
}
// </tree_lowerBound_is_list_lowerBound>

// <bst_is_sorted>
lemma void bst_is_sorted<V>(Tree<int,V> t)
  requires bst(t) == true;
  ensures sorted(flatten(t)) == true;
{
  switch (t) {
    case Leaf:
    case Branch(l, k, v, c, r):
      bst_is_sorted(l);
      bst_is_sorted(r);
      tree_higherBound_is_list_higherBound(l, k);
      tree_lowerBound_is_list_lowerBound(k, r);
      sorted_sides_sorted_whole(flatten(l), k, v, flatten(r));
  }
}
// </bst_is_sorted>

@*/

#endif
#ifndef LLRB_LEMMAS_H
#define LLRB_LEMMAS_H

#include "../../../../../spec/index.h"
#include "../flatten.h"
#include "./predicate.h"
#include "../fixpoints.h"

/*@
lemma void black_with_height_has_elements<K,V>(Tree<K,V> t0)
  requires IsLLRB(t0, Blk, ?h)  &*&  0 < h;
  ensures  IsLLRB(t0, Blk,  h)  &*&  Cons(flatten(t0), _, _);
{
  open IsLLRB(t0, Blk, h);
  close Cons(flatten(t0), _, _);
  close IsLLRB(t0, Blk, h);
}

lemma void red_has_elements<K,V>(Tree<K,V> t0)
  requires IsLLRB(t0, Red, ?h);
  ensures  IsLLRB(t0, Red,  h)  &*&  Cons(flatten(t0), _, _);
{
  open IsLLRB(t0, Red, h);
  close Cons(flatten(t0), _, _);
  close IsLLRB(t0, Red, h);
}


lemma void red_has_branch<K,V>(Tree<K,V> t);
  requires IsLLRB(t, Red, ?h);
  ensures  IsLLRB(t, Red,  h)  &*&  is_branch(t) == true;
  

lemma void llrb_emp<K,V>(Tree<K,V> tree)
  requires IsLLRB(tree, _, _);
  ensures emp;
{
  open IsLLRB(tree, _, _);
  switch (tree) {
    case Leaf:
    case Branch(l, k, v, c, r):
      llrb_emp(l);
      llrb_emp(r);
  }
}

lemma void IsLLRB_dup<K,V>(Tree<K,V> T)
  requires IsLLRB(T, ?c, ?h);
  ensures  IsLLRB(T,  c,  h) &*& IsLLRB(T,  c,  h);
{
  open IsLLRB(T, c, h);
  switch (T) {
    case Leaf:
      close IsLLRB(T, c, h);
      close IsLLRB(T, c, h);
    case Branch(L,k,v,c',R):
      IsLLRB_dup(L);
      IsLLRB_dup(R);
      close IsLLRB(T, c, h);
      close IsLLRB(T, c, h);
  }
}

lemma void height_gte_0<K,V>(Tree<K,V> tree)
  requires IsLLRB(tree, ?c, ?h);
  ensures  IsLLRB(tree,  c,  h)  &*& h >= 0;
{
  open IsLLRB(tree, c, h);
  switch (tree) {
    case Leaf:
      close IsLLRB(tree, c, h);
    case Branch(l, k, v, c', r):
      height_gte_0(l);
      close IsLLRB(tree, c, h);
  }
}

lemma void red_is_branch<K,V>(Tree<K,V> t)
  requires IsLLRB(t, Red, ?h);
  ensures  IsLLRB(t, Red,  h)  &*&  Branch(t, _, _, _, Red, _);
{
  open IsLLRB(t, Red, h);
  switch(t) {
    case Leaf: return false;
    case Branch(l, k, v, c, r):
      close Branch(t, l, k, v, Red, r);
      close IsLLRB(t, Red, h);
  }
}

lemma void black_0_is_leaf<K,V>(Tree<K,V> t)
  requires IsLLRB(t, Blk, 0);
  ensures  IsLLRB(t, Blk, 0)  &*&  t == Leaf;
{
  switch (t) {
    case Leaf:
    case Branch(l, k, v, c, r):
      open IsLLRB(t, Blk, 0);
      height_gte_0(l);
      close IsLLRB(t, Blk, 0);
  }
}

lemma void black_0_has_no_elements<K,V>(Tree<K,V> t0)
  requires IsLLRB(t0, Blk, 0);
  ensures  IsLLRB(t0, Blk, 0)  &*&  flatten(t0) == nil;
{
  black_0_is_leaf(t0);
}


lemma void b3_or_b2<K,V>(Tree<K,V> t, bool fix)
  requires  Branch(t, ?l, ?k, ?v, Blk, ?r)
       &*&  IsLLRB(l, (fix ? Blk : Red), ?h)
       &*&  IsLLRB(r, Blk, h);
  ensures IsLLRB(t, Blk, h+1);
{
  open Branch(t, l, k, v, Blk, r);
  if (fix) {
    close IsLLRB(t, Blk, h+1);
  }
  else {
    close IsLLRB(t, Blk, h+1);
  }
}

@*/

#endif
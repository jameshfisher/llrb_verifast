#ifndef IsLLRB_NODE_LEMMAS_H
#define IsLLRB_NODE_LEMMAS_H

#include "../../data/node.h"
#include "./predicate.h"
#include "./lemmas.h"

/*@
lemma void black_non_null_has_height<K,V>(TreeNodePtr root)
  requires IsTree(root, ?t)  &*&  IsLLRB(t, Blk, ?h)  &*&  root != 0;
  ensures  IsTree(root,  t)  &*&  IsLLRB(t, Blk,  h)  &*&  0 < h;
{
  switch (t) {
    case Leaf:
      open IsTree(root, t);
      close IsTree(root, t);
    case Branch(l, k, v, c, r):
      open IsLLRB(t, Blk, h);
      height_gte_0(l);
      close IsLLRB(t, Blk, h);
  }
}

lemma void null_ptr_is_black_Leaf(TreeNodePtr root)
  requires IsTree(root, ?t)  &*&  IsLLRB(t, ?c, ?h)  &*&  root == 0;
  ensures  IsTree(root,  t)  &*&  IsLLRB(t,  c,  h)  &*&  c == Blk  &*&  h == 0;
{
  open IsTree(root, t);
  open IsLLRB(t, c, h);
  close IsLLRB(t, c, h);
  close IsTree(root, t);
}

lemma void dispose_null_ptr(TreeNodePtr root)
  requires IsTree(root, ?t)  &*&  IsLLRB(t, ?c, ?h)  &*&  root == 0;
  ensures emp  &*&  c == Blk  &*&  h == 0;
{
  null_ptr_is_black_Leaf(root);
  open IsTree(root, t);
  open IsLLRB(t, Blk, 0);
}

lemma void dispose_black_0(TreeNodePtr root)
  requires IsTree(root, ?t)  &*&  IsLLRB(t, Blk, 0);
  ensures emp;
{
  black_0_is_leaf(t);
  open IsLLRB(t, Blk, 0);
  open IsTree(root, t);
}
@*/

#endif
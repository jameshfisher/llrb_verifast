#ifndef LLRB_DATATYPE_H
#define LLRB_DATATYPE_H

#include "../tree.h"

/*@
// <IsLLRB_predicate>
predicate IsLLRB<K,V>(Tree<K,V> t; Color c, int h) =
  switch (t) {
    case Leaf: return
      h == 0  &*&
      c == Blk;
    case Branch(l, k, v, c', r): return
      c == c'  &*&
      IsLLRB(l, ?lftC, ?subH)  &*&
      IsLLRB(r,  Blk,   subH)  &*&
      ( c == Red ?     h == subH    &*&  lftC == Blk
      : c == Blk  &*&  h == subH+1);
  };
// </IsLLRB_predicate>
@*/

#endif

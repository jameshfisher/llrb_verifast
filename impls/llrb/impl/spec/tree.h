#ifndef IMPLS_BST_SPEC_TREE_H
#define IMPLS_BST_SPEC_TREE_H

// <Color>
enum Color { Red, Blk };
typedef enum Color Color;
// </Color>

/*@
// <Tree>
inductive Tree<K,V> =
  | Leaf
  | Branch(Tree<K,V>, K, V, Color, Tree<K,V>);
// </Tree>

// <Branch_predicate>
predicate Branch<K,V>(Tree<K,V> t, Tree<K,V> l, K k, V v, Color c, Tree<K,V> r) =
  switch (t) {
    case Leaf: return false;
    case Branch(l0, k0, v0, c0, r0):
      return
        l == l0  &*&
        k == k0  &*&
        v == v0  &*&
        c == c0  &*&
        r == r0;
  };
// </Branch_predicate>

@*/

#endif
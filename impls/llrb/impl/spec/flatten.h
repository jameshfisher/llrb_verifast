#ifndef DATA_FLATTEN_H
#define DATA_FLATTEN_H

#include "../../../../spec/index.h"
#include "./tree.h"

/*@
// <flatten>
fixpoint list<Pair<K,V> > flatten<K,V>(Tree<K,V> t) {
  switch (t) {
    case Leaf: return nil;
    case Branch(l, k, v, c, r): return append(flatten(l), cons(Pair(k,v), flatten(r)));
  }
}
// </flatten>
@*/

#endif
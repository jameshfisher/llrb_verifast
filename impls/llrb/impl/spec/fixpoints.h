#ifndef SPEC_FIXPOINTS_H
#define SPEC_FIXPOINTS_H

#include "./tree.h"

/*@
fixpoint bool is_branch<K,V>(Tree<K,V> t) {
  switch (t) {
    case Leaf: return false;
    case Branch(l,k,v,c,r): return true;
  }
}

fixpoint bool has_left_branch<K,V>(Tree<K,V> t) {
  switch (t) {
    case Leaf: return false;
    case Branch(l,k,v,c,r): return is_branch(l);
  }
}

fixpoint bool has_right_branch<K,V>(Tree<K,V> t) {
  switch (t) {
    case Leaf: return false;
    case Branch(l, k, v, c, r): return is_branch(r);
  }
}

fixpoint bool has_right_left_branch<K,V>(Tree<K,V> t) {
  switch(t) {
    case Leaf: return false;
    case Branch(l,k,v,c,r): return has_left_branch(r);
  }
}

fixpoint bool has_left_right_branch<K,V>(Tree<K,V> t) {
  switch(t) {
    case Leaf: return false;
    case Branch(l,k,v,c,r): return has_right_branch(l);
  }
}

@*/

#endif